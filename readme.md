# Final Java Project for Epam 
Project: Ads. Buy ,sell, rent, exchange different goods.  
Technology:  

   * Back-end  : JavaEE: Servlet, JSP, JSTL,  
   * Front-end : HTML, CSS, JS  
  
  Servlet container: Tomcat 8.5  
  Data base: MySQL , JDBC  
  Logger: Log4J2  
  Tests: TestNG  
  Build tool: Maven  
  Developer: Dmitry Krivolap  

1. Guest
    * Sign in
    * Sign up
    * Recover password(send new password by email)
    * View themes posts
    * View posts (with pagination)
    * View About page
    * View User information
    * Search posts by: (view with pagination)
        * mini description and content
        * type post ('BUY','SELL','RENT','EXCHANGE')
        * min price
        * max price
        
2. Client
    * Edit profile
    * Create post
    * Edit post
    * Delete post
    * Create comment
    * Delete comment
    * View messages (with pagination)
    * Send message
    * Set Mark (change user's rating)
    * View self rating
    
3. Admin
    * Delete any post
    * Edit any post
    * Delete any comment
    * Block any client
    * Unlock any client