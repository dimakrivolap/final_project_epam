function validatePassword(){
    var password = document.getElementById("password");
    var repassword = document.getElementById("repassword");

    if((password.value != repassword.value)||(password.value=="")) {
        repassword.setCustomValidity("Passwords Don't Match");
    } else {
        repassword.setCustomValidity('');
    }
}