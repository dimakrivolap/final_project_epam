<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="common/meta.jsp" %>
    <%@ include file="common/header.jsp" %>
    <title>${user.login}</title>
</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="common/navbar.jsp" %>
        <%@ include file="parts/user-body.jsp" %>
    </div>
</div>
<%@ include file="common/footer.jsp" %>
</body>
</html>
