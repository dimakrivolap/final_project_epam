<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="common/meta.jsp" %>
    <%@ include file="common/header.jsp" %>
    <title><fmt:message key="label.password.retrieval" bundle="${rb}"/></title>
</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="common/navbar.jsp" %>
        <div class="content">
            <%@ include file="parts/retrieval-body.jsp" %>
        </div>
    </div>
</div>
<%@ include file="common/footer.jsp" %>
</body>
</html>