<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setLocale value="${sessionScope.locale}" scope="session" />
<fmt:setBundle basename="pagecontent" var="rb" />
<%@taglib uri="customtags" prefix="f" %>



<link href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
<link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="/assets/css/main.css">
<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/script.js"></script>
<script src="${pageContext.request.contextPath}/assets/js/ValidatorPassword.js"></script>