<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
    <form action="controller" method="post">
        <select id="selectLocale" onchange="changeFunc();">
            <option value="EN" ${sessionScope.locale == 'en_US' ? 'selected' : ''}><a href="#">English</a></option>
            <option value="RU" ${sessionScope.locale == 'ru_RU' ? 'selected' : ''}><a href="#">Russian</a></option>
            <option value="DE" ${sessionScope.locale == 'de_DE' ? 'selected' : ''}><a href="#">Deutsch</a></option>
        </select>
    </form>
    <div class="container">
        <a class="navbar-brand" href="/main">Ads</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/main"><fmt:message key="label.main.title" bundle="${rb}"/></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/controller/posts?command=get_all_themes"><fmt:message key="label.posts.title" bundle="${rb}"/></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/controller?command=get_search_page"><fmt:message key="label.search.title" bundle="${rb}"/></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/controller?command=about"><fmt:message key="label.about.title" bundle="${rb}"/></a>
        </li>
        <c:if test="${not empty sessionScope.user}">
            <li class="nav-item">
                    <a class="nav-link" href="/controller?command=get_messages&start=0"><fmt:message key="label.messages.title" bundle="${rb}"/></a>
            </li>
            <li>
                <a class="nav-link" href="/controller?command=logout"><fmt:message key="label.logout" bundle="${rb}"/></a>
            </li>
        </c:if>
    </ul>
    </div>
</nav>