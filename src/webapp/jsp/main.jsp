<%--
  Created by IntelliJ IDEA.
  User: Дима Криволап
  Date: 17.09.2018
  Time: 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
    <c:when test="${not empty sessionScope.user.login}">
        <%@include file="client/clientpage.jsp"%>
    </c:when>
    <c:otherwise>
        <%@include file="login.jsp"%>
    </c:otherwise>
</c:choose>

