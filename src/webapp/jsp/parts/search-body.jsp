<div class="input-group mb-3">
    <form name="searchForm" method="GET" action="/controller/posts">
        <input type="hidden" name="command" value="find_post"/>
        <fmt:message key="label.search.title" bundle="${rb}"/>:<br/>
        <input type="text" name="search_phrase" value=""/>
        <br/>
        <fmt:message key="label.typepost" bundle="${rb}"/>:<br/>
        <select name="type_post">
            <option selected value="SELL"><fmt:message key="label.typepost.sell" bundle="${rb}"/></option>
            <option value="BUY"><fmt:message key="label.typepost.buy" bundle="${rb}"/></option>
            <option value="RENT"><fmt:message key="label.typepost.rent" bundle="${rb}"/></option>
            <option value="EXCHANGE"><fmt:message key="label.typepost.exchange" bundle="${rb}"/></option>
        </select>

        <br/><fmt:message key="label.price" bundle="${rb}"/>:<br/>
        <input type="text" name="price_min" placeholder="min" value=""/>-
        <input type="text" name="price_max" placeholder="max" value=""/>
        <br/>
        <input type="hidden" name="start" value="0"/>
        <input class="btn btn-primary" type="submit" value="<fmt:message key="label.search.title" bundle="${rb}"/>"/>
    </form>
</div>