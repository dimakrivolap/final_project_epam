<h3><fmt:message key="label.register.title" bundle="${rb}"/></h3>
<hr/>
<form name="registrationForm" method="POST" action="/controller/registration">
    <input type="hidden" name="command" value="registration"/>
    <fmt:message key="label.login.title" bundle="${rb}"/>:<br/>
    <input type="text" name="login" value="" pattern="^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$" required/>
    <br/><fmt:message key="label.password" bundle="${rb}"/>:<br/>
    <input type="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="password" onsubmit="validatePassword();" onchange="validatePassword();" value="" />
    <br/><fmt:message key="label.retype_password" bundle="${rb}"/>:<br/>
    <input type="password" id="repassword" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="repassword" onkeyup="validatePassword();" value="" />
    <br/>
    <br/><fmt:message key="label.email" bundle="${rb}"/>:<br/>
    <input type="text" name="email" value="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required/>
    <br/><fmt:message key="label.first_name" bundle="${rb}"/>:<br/>
    <input type="text" name="first_name" value="" required/>
    <br/><fmt:message key="label.last_name" bundle="${rb}"/>:<br/>
    <input type="text" name="last_name" value="" required/>
    <br/><fmt:message key="label.phone_number" bundle="${rb}"/>:<br/>
    <input type="text" name="phone_number" pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$" value="" required/>
    <br/>
    ${errorLoginPassMessage}
    <br/>
    ${errorRegisterPassMessage}
    <br/>
    ${errorRegistrationMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.signup" bundle="${rb}"/>"/>
</form>
<a class="btn btn-primary" href="/main"><fmt:message key="label.home" bundle="${rb}"/></a>
<hr/>