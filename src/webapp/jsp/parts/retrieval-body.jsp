<div class="input-group mb-3">
    <form name="retrievalPassword" method="POST" action="/controller">
        <input type="hidden" name="command" value="password_retrieval"/>
        <fmt:message key="label.email" bundle="${rb}"/>:<br/>
        <input type="text" name="email" value=""/>
        <br/>
        <input class="btn btn-primary" type="submit" value="<fmt:message key="label.restore" bundle="${rb}"/>"/>
    </form>
</div>