<h1> <fmt:message key="label.messages.title" bundle="${rb}"/></h1>
<a class="btn btn-primary" href="/controller?command=get_messages&type_messages=receiver&start=0"><fmt:message key="label.messages.received.title" bundle="${rb}"/></a>
<a class="btn btn-primary" href="/controller?command=get_messages&type_messages=sent&start=0"><fmt:message key="label.messages.sent.title" bundle="${rb}"/></a>

<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">ico_sender</th>
        <th scope="col">sender login</th>
        <th scope="col">text</th>
        <th scope="col"><fmt:message key="label.date_created" bundle="${rb}"/></th>
        <th scope="col">answer</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${messages}" var="message">
        <tr>
            <td>${message.id}</td>
            <th><img src="/image/user/${message.idUserSender}" alt="image for User" width="64" height=64"></th>
            <c:forEach items="${users}" var="userSender">
                <c:if test="${userSender.id==message.idUserSender}">
                    <c:set var = "sender" scope = "session" value = "${userSender.login}"/>
                </c:if>
            </c:forEach>
            <td>${sender}</td>
            <td>${message.text}</td>
            <td>${f:formatLocalDateTime(message.dateCreated, 'dd.MM.yyyy HH:mm:ss')}</td>
            <td><a class="btn btn-primary" href="/controller?command=send_message_page&receiver=${sender}"><fmt:message key="label.sendMessage.title" bundle="${rb}"/></a></td>

        </tr>
    </c:forEach>
    </tbody>
</table>
<c:if test = "${start > 0}">
    <a class="btn btn-primary" href="/controller/posts?command=get_messages&type_messages=${type_messages}&start=${start-5}"><fmt:message key="label.previous" bundle="${rb}"/></a>
</c:if>
<a class="btn btn-primary" href="/controller/posts?command=get_messages&type_messages=${type_messages}&start=${start}"><fmt:message key="label.page" bundle="${rb}"/> ${current_page}</a>
<c:if test = "${current_page < count_pages}">
    <a  class="btn btn-primary" href="/controller/posts?command=get_messages&type_messages=${type_messages}&start=${start+5}"><fmt:message key="label.next" bundle="${rb}"/></a>
</c:if>
