<form name="newCommentForm" method="POST" action="/controller/main">
    <input type="hidden" name="command" value="create_comment"/>
    <input type="hidden" name="id_post" value="${post.id}"/>
    <fmt:message key="label.comment" bundle="${rb}"/>:<br/>
    <input type="text" name="text" value=""/><br/>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.publish" bundle="${rb}"/>"/>
</form>
