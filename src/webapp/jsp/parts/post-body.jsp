<h1>Owner</h1>
<img src="/image/user/${user_owner.id}" width="200" height="200">
<c:if test="${not empty sessionScope.user && sessionScope.user.id ne user_owner.id}">
    <a class="btn btn-primary" href="/controller?command=send_message_page&receiver=${user_owner.login}">send message</a>
</c:if>
<a class="btn btn-primary" href="/controller?command=get_user&id=${user_owner.id}">${user_owner.login}</a>
<h3>${user_owner.phoneNumber}</h3>


<h1>Post</h1>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">idThemePost</th>
        <th scope="col"><fmt:message key="label.typepost" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.minidescription" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.context" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.price" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.date_created" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.last_updated" bundle="${rb}"/></th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">${post.id}</th>
            <td>${post.idThemePost}</td>
            <td>${post.typePost}</td>
            <td>${post.miniDescription}</td>
            <td>${post.context}</td>
            <td>${post.price}</td>
            <td>${f:formatLocalDateTime(post.dateCreated, 'dd.MM.yyyy HH:mm:SS')}</td>
            <td>${f:formatLocalDateTime(post.lastUpdate, 'dd.MM.yyyy HH:mm:SS')}</td>
        </tr>
    </tbody>

    <img src="/image/post/${post.id}" alt="image for post ${post.id}" width="200" height="200">

</table>
