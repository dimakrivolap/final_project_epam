<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h1><fmt:message key="label.userpage" bundle="${rb}"/></h1>
<img src="/image/user/${user.id}" width="200" height="200">
<c:if test="${not empty sessionScope.user}">
    <div id="idReceiver" style="display: none;">${user.id}</div>
    <c:if test="${sessionScope.user.id != user.id}">
        <br/>
        <br/>
        <a class="btn btn-primary" href="/controller?command=send_message_page&receiver=${user.login}"><fmt:message key="label.sendMessage.title" bundle="${rb}"/></a>
        <br/>
        <br/>
        <select id="selectMark" onchange="changeMark();">
            <option value="" ${empty mark ? 'selected' : ''}><fmt:message key="label.choose" bundle="${rb}"/></option>
            <option value="1" ${mark == 1 ? 'selected' : ''}>1</option>
            <option value="2" ${mark == 2 ? 'selected' : ''}>2</option>
            <option value="3" ${mark == 3 ? 'selected' : ''}>3</option>
            <option value="4" ${mark == 4 ? 'selected' : ''}>4</option>
            <option value="5" ${mark == 5 ? 'selected' : ''}>5</option>
        </select>
    </c:if>
    <c:if test="${(role == 'ADMIN' && (user.typeUser != 'ADMIN'))}">
        <br/>
        <br/>
        <c:if test="${user.available}">
            <a class="btn btn-primary" href="/controller?command=CHANGE_AVAILABLE&id_user=${user.id}&available=false"><fmt:message key="label.block" bundle="${rb}"/></a>
        </c:if>
        <c:if test="${not user.available}">
            <a class="btn btn-primary" href="/controller?command=CHANGE_AVAILABLE&id_user=${user.id}&available=true"><fmt:message key="label.unlock" bundle="${rb}"/></a>
        </c:if>
    </c:if>
</c:if>
<h3>Rating:${rating}</h3>
<h3>${user.login}</h3>
<h3>${user.typeUser}</h3>
<h3>${user.firstName} ${user.lastName}</h3>
<h3>${user.email}</h3>
<h3>${user.phoneNumber}</h3>
<h3>Available: ${user.available}</h3>