<div class="input-group mb-3">
    <form name="loginForm" method="POST" action="/controller/main">
        <input type="hidden" name="command" value="login"/>
        <fmt:message key="label.login.title" bundle="${rb}"/>:<br/>

        <input type="text" name="login" value="" pattern="^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$"/>

        <br/><fmt:message key="label.password" bundle="${rb}"/>:<br/>
        <input type="password" name="password" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"/>
        <br/>
        <a href="/retrieval"><fmt:message key="label.password.retrieval" bundle="${rb}"/></a>
        ${errorLoginPassMessage}
        <br/>
        ${result}
        <br/>
        ${wrongAction}
        <br/>
        ${nullPage}
        <br/>
        <input class="btn btn-primary" type="submit" value="<fmt:message key="label.signin" bundle="${rb}"/>"/>

    </form>

</div>

<a class="btn btn-primary" href="/registration"><fmt:message key="label.signup" bundle="${rb}"/></a>
<hr/>