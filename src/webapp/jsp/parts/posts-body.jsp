<h1>Posts By Theme ${theme.name}</h1>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">ico</th>
        <th scope="col"><fmt:message key="label.typepost" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.minidescription" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.price" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.date_created" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.last_updated" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.info" bundle="${rb}"/></th>
        <c:if test="${(not empty sessionScope.user && (sessionScope.user.id == comment.idUser)||(role=='ADMIN'))}">
            <th scope="col"><fmt:message key="label.change" bundle="${rb}"/></th>
            <th scope="col"><fmt:message key="label.delete" bundle="${rb}"/></th>
        </c:if>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${posts}" var="post">
        <tr>
            <th><img src="/image/post/${post.id}" alt="image for post ${post.id}" width="32" height=32"></th>
            <td>${post.typePost}</td>
            <td>${post.miniDescription}</td>
            <td>${post.price}</td>
            <td>${f:formatLocalDateTime(post.dateCreated, 'dd.MM.yyyy hh:mm:ss')}</td>
            <td>${f:formatLocalDateTime(post.lastUpdate, 'dd.MM.yyyy hh:mm:ss')}</td>
            <td><a class="btn btn-primary" href="/controller/posts?command=get_post&id=${post.id}"><fmt:message key="label.info" bundle="${rb}"/></a></td>
            <c:if test="${(not empty sessionScope.user && (sessionScope.user.id == comment.idUser)||(role=='ADMIN'))}">
                <td><a class="btn btn-primary" href="/controller/posts?command=modify_post&id=${post.id}"><fmt:message key="label.change" bundle="${rb}"/></a></td>
                <td><a class="btn btn-primary" href="/controller/posts?command=delete_post&id=${post.id}"><fmt:message key="label.delete" bundle="${rb}"/></a></td>
            </c:if>
        </tr>
    </c:forEach>
    </tbody>
</table>
<c:if test="${empty search}">
    <c:if test="${start > 0}">
        <a class="btn btn-primary" href="/controller/posts?command=get_posts&theme=${theme.id}&start=${start-5}"><fmt:message key="label.previous" bundle="${rb}"/></a>
    </c:if>
    <a class="btn btn-primary"
       href="/controller/posts?command=get_posts&theme=${theme.id}&start=${start}"><fmt:message key="label.page" bundle="${rb}"/> ${current_page}</a>
    <c:if test="${current_page < count_pages}">
        <a class="btn btn-primary"
           href="/controller/posts?command=get_posts&theme=${theme.id}&start=${start+5}"><fmt:message key="label.next" bundle="${rb}"/></a>
    </c:if>
</c:if>

<c:if test="${search}">
    <c:if test="${start > 0}">
        <a class="btn btn-primary" href="${url}&start=${start-5}"><fmt:message key="label.previous" bundle="${rb}"/></a>
    </c:if>
    <a class="btn btn-primary" href="${url}&start=${start}"><fmt:message key="label.page" bundle="${rb}"/> ${current_page}</a>
    <c:if test="${current_page < count_pages}">
        <a class="btn btn-primary"
           href="${url}&start=${start+5}"><fmt:message key="label.next" bundle="${rb}"/></a>
    </c:if>
</c:if>

<br/>