<h1>Comments</h1>
<c:if test="${not empty sessionScope.user}">
    <%@ include file="new_comment-body.jsp" %>
</c:if>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">icoUser</th>
        <th scope="col"><fmt:message key="label.login.title" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.text" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.last_updated" bundle="${rb}"/></th>
        <c:if test="${(not empty sessionScope.user && (sessionScope.user.id == comment.idUser)||(role=='ADMIN'))}">
            <th scope="col"><fmt:message key="label.delete" bundle="${rb}"/></th>
        </c:if>
        <c:if test="${not empty sessionScope.user && sessionScope.user.id ne comment.idUser}">
            <th scope="col"><fmt:message key="label.sendMessage.title" bundle="${rb}"/></th>
        </c:if>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${comments}" var="comment">
        <tr>
            <th><img src="/image/user/${comment.idUser}" alt="image for post ${comment.id}" width="48" height=48"></th>
            <c:forEach items="${commentators}" var="commentator">
                <c:if test="${commentator.id==comment.idUser}">
                    <c:set var="com" scope="session" value="${commentator}"/>
                </c:if>
            </c:forEach>
            <td><a class="btn btn-primary" href="/controller?command=get_user&id=${com.id}">${com.login}</a></td>
            <td>${comment.text}</td>
            <td>${f:formatLocalDateTime(comment.lastUpdate, 'dd.MM.yyyy HH:MM:SS')}</td>
            <c:if test="${(not empty sessionScope.user && (sessionScope.user.id == comment.idUser)||(role=='ADMIN'))}">
                <td><a class="btn btn-primary"
                       href="/controller/posts?command=delete_comment&id_comment=${comment.id}&id_post=${post.id}"><fmt:message key="label.delete" bundle="${rb}"/></a>
                </td>
            </c:if>
            <c:if test="${not empty sessionScope.user && sessionScope.user.id ne comment.idUser}">
                <td><a class="btn btn-primary" href="/controller?command=send_message_page&receiver=${com.login}"><fmt:message key="label.sendMessage.title" bundle="${rb}"/></a></td>
            </c:if>
        </tr>
    </c:forEach>
    </tbody>
</table>
