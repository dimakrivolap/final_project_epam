<h1><a id="Final_Java_Project_for_Epam_0"></a>Final Java Project for Epam</h1>
<p>Project: Ads. Buy ,sell, rent, exchange different goods.<br>
    Technology:</p>
<ul>
    <li>Back-end  : JavaEE: Servlet, JSP, JSTL,</li>
    <li>Front-end : HTML, CSS, JS</li>
</ul>
<p>Servlet container: Tomcat 8.5 Data base: MySQL , JDBC<br>
    Logger: Log4J2<br>
    Tests: TestNG<br>
    Build tool: Maven<br>
    Developer: Dmitry Krivolap</p>
<ol>
    <li>
        <p>Guest</p>
        <ul>
            <li>Sign in</li>
            <li>Sign up</li>
            <li>Recover password(send new password by email)</li>
            <li>View themes posts</li>
            <li>View posts (with pagination)</li>
            <li>View About page</li>
            <li>View User information</li>
            <li>Search posts by: (view with pagination)
                <ul>
                    <li>mini description and content</li>
                    <li>type post (‘BUY’,‘SELL’,‘RENT’,‘EXCHANGE’)</li>
                    <li>min price</li>
                    <li>max price</li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <p>Client</p>
        <ul>
            <li>Edit profile</li>
            <li>Create post</li>
            <li>Edit post</li>
            <li>Delete post</li>
            <li>Create comment</li>
            <li>Delete comment</li>
            <li>View messages (with pagination)</li>
            <li>Send message</li>
            <li>Set Mark (change user’s rating)</li>
            <li>View self rating</li>
        </ul>
    </li>
    <li>
        <p>Admin</p>
        <ul>
            <li>Delete any post</li>
            <li>Edit any post</li>
            <li>Delete any comment</li>
            <li>Block any client</li>
            <li>Unlock any client</li>
        </ul>
    </li>
</ol>