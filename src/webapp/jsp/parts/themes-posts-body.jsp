<h1><fmt:message key="label.posts.title" bundle="${rb}"/></h1>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">name</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${themes}" var="theme">
        <tr>
            <th scope="row"><a href="/controller/posts?command=get_posts&theme=${theme.id}&start=0">${theme.name}</a></th>
        </tr>
    </c:forEach>
    </tbody>
</table>