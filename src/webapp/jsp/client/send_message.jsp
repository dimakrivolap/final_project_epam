<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.epam.ads.controller.TypeUploadImage" %>
<html>
<head>
    <%@ include file="../common/meta.jsp" %>
    <%@ include file="../common/header.jsp" %>
    <title><fmt:message key="label.sendMessage.title" bundle="${rb}"/></title>

</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="../common/navbar.jsp"%>
        <%@ include file="parts/send_message-body.jsp"%>
    </div>
</div>
<%@ include file="../common/footer.jsp" %>
</body>
</html>
