<h1>Posts</h1>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">ico</th>
        <th scope="col"><fmt:message key="label.typepost" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.minidescription" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.context" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.price" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.date_created" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.last_updated" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.info" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.change" bundle="${rb}"/></th>
        <th scope="col"><fmt:message key="label.delete" bundle="${rb}"/></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${posts}" var="post">
        <tr>
            <th><img src="/image/post/${post.id}" alt="image for post ${post.id}" width="64" height=64"></th>
            <td>${post.typePost}</td>
            <td>${post.miniDescription}</td>
            <td>${post.context}</td>
            <td>${post.price}</td>
            <td>${f:formatLocalDateTime(post.dateCreated, 'dd.MM.yyyy HH:mm:ss')}</td>
            <td>${f:formatLocalDateTime(post.lastUpdate, 'dd.MM.yyyy HH:mm:ss')}</td>
            <td><a class="btn btn-primary" href="/controller/posts?command=get_post&id=${post.id}"><fmt:message key="label.info" bundle="${rb}"/></a></td>
            <td><a class="btn btn-primary" href="/controller/posts?command=modify_post&id=${post.id}"><fmt:message key="label.change" bundle="${rb}"/></a></td>
            <td><a class="btn btn-primary" href="/controller/posts?command=delete_post&id=${post.id}"><fmt:message key="label.delete" bundle="${rb}"/></a></td>
        </tr>
    </c:forEach>
    </tbody>
    <a class="btn btn-primary" href="/controller/posts?command=new_post"><fmt:message key="label.new_post" bundle="${rb}"/></a>
</table>