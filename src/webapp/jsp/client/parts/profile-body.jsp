<h3><fmt:message key="label.profile.title" bundle="${rb}"/></h3>
<form name="registrationForm" method="POST" action="/controller/profile">
    <input type="hidden" name="command" value="update_profile"/>
    <br/><fmt:message key="label.old_password" bundle="${rb}"/>:<br/>
    <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="old_password" value="" required/>
    <br/><fmt:message key="label.new_password" bundle="${rb}"/>:<br/>
    <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="password" onchange="validatePassword();" value="" required/>
    <br/><fmt:message key="label.retype_password" bundle="${rb}"/>:<br/>
    <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" name="repassword" onkeyup="validatePassword();" value="" required/>
    <br/>
    <br/><fmt:message key="label.email" bundle="${rb}"/>Email:<br/>
    <input type="text" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="${sessionScope.user.email}" required/>
    <br/><fmt:message key="label.first_name" bundle="${rb}"/>:<br/>
    <input type="text" name="first_name" value="${sessionScope.user.firstName}" required/>
    <br/><fmt:message key="label.last_name" bundle="${rb}"/>:<br/>
    <input type="text" name="last_name" value="${sessionScope.user.lastName}" required/>
    <br/><fmt:message key="label.phone_number" bundle="${rb}"/>:<br/>
    <input type="text" name="phone_number" pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$" value="${sessionScope.user.phoneNumber}" required/>
    <br/>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.update_profile" bundle="${rb}"/>"/>
</form>
<img src="${pageContext.request.contextPath}/image/user/${user.id}" width="200" height="200">
<form name="uploadImage" id="uploadImage" method="POST" action="/upload" enctype="multipart/form-data">
    <br/><fmt:message key="label.photo" bundle="${rb}"/><br/>
    <input  type="hidden" name="typeUploadImage" value="user">
    <input type="file" name="data"><br>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.update_image" bundle="${rb}"/>"/>
    ${errorRegisterPassMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
</form>
<hr/>