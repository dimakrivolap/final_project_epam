<h3><fmt:message key="label.newpost.title" bundle="${rb}"/></h3>
<hr/>
<form name="newPostForm" method="POST" action="/controller/post">
    <input type="hidden" name="command" value="create_post"/>
    <fmt:message key="label.themepost.title" bundle="${rb}"/>:<br/>
    <select name="theme_post" required>
    <c:forEach items="${themes}" var="theme">
        <option value="${theme.id}">${theme.name}</option>
    </c:forEach>
    </select>
    <br/><fmt:message key="label.typepost" bundle="${rb}"/>:<br/>
    <select name="type_post" required>
        <option selected value="SELL"><fmt:message key="label.typepost.sell" bundle="${rb}"/></option>
        <option value="BUY"><fmt:message key="label.typepost.buy" bundle="${rb}"/></option>
        <option value="RENT"><fmt:message key="label.typepost.rent" bundle="${rb}"/></option>
        <option value="EXCHANGE"><fmt:message key="label.typepost.exchange" bundle="${rb}"/></option>
    </select>
    <br/><fmt:message key="label.minidescription" bundle="${rb}"/>:<br/>
    <input type="text" name="mini_description" value="" maxlength="255" required/>
    <br/>
    <br/><fmt:message key="label.price" bundle="${rb}"/>:<br/>
    <input type="text" name="price" pattern="\d+(\.\d{1,2})?" value="" required/>
    <br/><fmt:message key="label.context" bundle="${rb}"/>:<br/>
    <input type="text" name="context" value="" required/>
    ${errorLoginPassMessage}
    <br/>
    ${errorRegisterPassMessage}
    <br/>
    ${errorRegistrationMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.publish" bundle="${rb}"/>"/>
</form>
<a class="btn btn-primary" href="/main"><fmt:message key="label.home" bundle="${rb}"/></a>
<hr/>