<h3><fmt:message key="label.newpost.title" bundle="${rb}"/></h3>
<hr/>
<form name="newMessageForm" method="POST" action="/controller">
    <br/>
    <input type="hidden" name="command" value="send_message"/>
    <br/>
    <fmt:message key="label.receiver" bundle="${rb}"/>:<br/>
    <input type="text" name="receiver" value="${receiver}" required/>
    <br/>
    <br/>
    <fmt:message key="label.text" bundle="${rb}"/>:<br/>
    <input type="text" name="text" value="" required/>
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.send" bundle="${rb}"/>"/>
</form>
<a class="btn btn-primary" href="/main"><fmt:message key="label.home" bundle="${rb}"/></a>
<hr/>