<img src="${pageContext.request.contextPath}/image/user/${user.id}" width="200" height="200">
<h3><fmt:message key="label.rating" bundle="${rb}"/>: ${sessionScope.rating}</h3>
<h1>${sessionScope.user.login} <fmt:message key="label.welcome" bundle="${rb}"/></h1>

    ${errorLoginPassMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${result}
    <br/>
    ${nullPage}


<a class="btn btn-primary" href="/controller?command=user_posts"><fmt:message key="label.my_posts" bundle="${rb}"/></a>

<a class="btn btn-primary" href="/profile"><fmt:message key="label.profile.title" bundle="${rb}"/></a>