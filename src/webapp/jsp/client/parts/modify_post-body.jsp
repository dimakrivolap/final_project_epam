<h3><fmt:message key="label.modifypost.title" bundle="${rb}"/></h3>
<hr/>
<form name="updatePostForm" method="POST" action="/controller/post">
    <input type="hidden" name="command" value="update_post"/>
    <input type="hidden" name="id" value="${post.id}"/>
    <fmt:message key="label.themepost.title" bundle="${rb}"/>:<br/>
    <select name="theme_post" required>
        <c:forEach items="${themes}" var="theme">
            <option value="${theme.id}" ${post.idThemePost == theme.id ? 'selected' : ''}>${theme.name}</option>
        </c:forEach>
    </select>
    <br/><fmt:message key="label.typepost" bundle="${rb}"/>:<br/>
    <select name="type_post" required>
        <option selected value="SELL" ${post.typePost == "SELL" ? 'selected' : ''}><fmt:message key="label.typepost.sell" bundle="${rb}"/></option>
        <option value="BUY" ${post.typePost == "BUY" ? 'selected' : ''}><fmt:message key="label.typepost.buy" bundle="${rb}"/></option>
        <option value="RENT" ${post.typePost == "RENT" ? 'selected' : ''}><fmt:message key="label.typepost.rent" bundle="${rb}"/></option>
        <option value="EXCHANGE" ${post.typePost == "EXCHANGE" ? 'selected' : ''}><fmt:message key="label.typepost.exchange" bundle="${rb}"/></option>
    </select>
    <br/><fmt:message key="label.minidescription" bundle="${rb}"/>:<br/>
    <input type="text" name="mini_description" maxlength="255" value="${post.miniDescription}" required/>
    <br/>
    <br/><fmt:message key="label.price" bundle="${rb}"/>:<br/>
    <input type="text" name="price"  pattern="\d+(\.\d{1,2})?" value="${post.price}" required/>
    <br/><fmt:message key="label.context" bundle="${rb}"/>:<br/>
    <input type="text" name="context" value="${post.context}" required/>
    ${errorLoginPassMessage}
    <br/>
    ${errorRegisterPassMessage}
    <br/>
    ${errorRegistrationMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.save" bundle="${rb}"/>"/>
</form>
<img src="${pageContext.request.contextPath}/image/post/${post.id}" width="200" height="200">
<form name="uploadImage" id="uploadImage" method="POST" action="/upload" enctype="multipart/form-data">
    <br/><fmt:message key="label.photo" bundle="${rb}"/><br/>
    <input  type="hidden" name="typeUploadImage" value="post">
    <input  type="hidden" name="postId" value="${post.id}">
    <input type="file" name="data"><br>
    <input class="btn btn-primary" type="submit" value="<fmt:message key="label.update_image" bundle="${rb}"/>"/>
    ${errorRegisterPassMessage}
    <br/>
    ${wrongAction}
    <br/>
    ${nullPage}
    <br/>
</form>

<a class="btn btn-primary" href="/controller?command=user_posts"><fmt:message key="label.my_posts" bundle="${rb}"/></a>
<br/>
<a class="btn btn-primary" href="/main"><fmt:message key="label.home" bundle="${rb}"/></a>
<hr/>