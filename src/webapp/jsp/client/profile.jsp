<%--
  Created by IntelliJ IDEA.
  User: Дима Криволап
  Date: 27.09.2018
  Time: 23:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="../common/meta.jsp" %>
    <%@ include file="../common/header.jsp" %>
    <title><fmt:message key="label.profile.title" bundle="${rb}"/></title>

</head>
<body>
    <div class="view">
        <div class="pages">
            <%@ include file="../common/navbar.jsp"%>
            <%@ include file="parts/profile-body.jsp"%>
        </div>
    </div>
    <%@ include file="../common/footer.jsp" %>
</body>
</html>
