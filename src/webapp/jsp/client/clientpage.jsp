<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <%@ include file="../common/header.jsp" %>
    <title><fmt:message key="label.userpage" bundle="${rb}"/></title>
</head>
<body>
    <div class="view">
        <div class="pages">
            <%@ include file="../common/navbar.jsp"%>
            <div class="content">
            <%@ include file="parts/clientpage-body.jsp"%>
            </div>
        </div>
    </div>
    <%@ include file="../common/footer.jsp" %>
</body>
</html>
