<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="../common/meta.jsp" %>
    <%@ include file="../common/header.jsp" %>
    <title><fmt:message key="label.modifypost.title" bundle="${rb}"/></title>

</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="../common/navbar.jsp"%>
        <%@ include file="parts/modify_post-body.jsp"%>
    </div>
</div>
<%@ include file="../common/footer.jsp" %>
</body>
</html>
