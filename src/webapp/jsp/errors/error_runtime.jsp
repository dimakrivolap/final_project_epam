<%--
  Created by IntelliJ IDEA.
  User: Дима Криволап
  Date: 17.09.2018
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="../common/meta.jsp" %>
    <%@ include file="../common/header.jsp" %>
    <title><fmt:message key="label.profile.title" bundle="${rb}"/></title>

</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="../common/navbar.jsp" %>
        <div class="content">
            Request from ${pageContext.errorData.requestURI} is failed
            <br/>
            Servlet name: ${pageContext.errorData.servletName}
            <br/>
            Status code: ${pageContext.errorData.statusCode}
            <br/>
            Exception: ${pageContext.exception}
            <br/>
            Message from exception: ${pageContext.exception.message}
        </div>
    </div>
</div>
<%@ include file="../common/footer.jsp" %>
</body>
</html>
