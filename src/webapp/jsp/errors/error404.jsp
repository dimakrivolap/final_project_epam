<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="../common/meta.jsp" %>
    <%@ include file="../common/header.jsp" %>
    <title><fmt:message key="label.profile.title" bundle="${rb}"/></title>

</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="../common/navbar.jsp" %>
        <div class="content">
            <h1><fmt:message key="label.error404" bundle="${rb}"/></h1>
        </div>
    </div>
</div>
<%@ include file="../common/footer.jsp" %>
</body>
</html>
