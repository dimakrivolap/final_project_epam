<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="common/meta.jsp" %>
    <%@ include file="common/header.jsp" %>
    <title>
        <fmt:message key="label.register.title" bundle="${rb}"/>
    </title>

</head>
<body>
    <div class="view">
        <div class="pages">
            <%@ include file="common/navbar.jsp"%>
            <%@ include file="parts/register-body.jsp"%>
        </div>
    </div>
    <%@ include file="common/footer.jsp" %>
</body>
</html>
