<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="common/meta.jsp" %>
    <%@ include file="common/header.jsp" %>
    <title><fmt:message key="label.posts.title" bundle="${rb}"/></title>
</head>
<body>
<div class="view">
    <div class="pages">
        <%@ include file="common/navbar.jsp" %>
        <%@ include file="parts/themes-posts-body.jsp" %>
    </div>
</div>
<%@ include file="common/footer.jsp" %>
</body>
</html>