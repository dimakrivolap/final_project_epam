package com.epam.ads.controller;

import com.epam.ads.command.ActionFactory;
import com.epam.ads.command.Command;
import com.epam.ads.connection.ConnectionPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "ControllerServlet",
        urlPatterns = {"/controller", "/controller/registration", "/controller/main", "/controller/posts", "/controller/profile", "/controller/post"}
)
/**
 * ControllerServlet.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see HttpServlet
 */
public class ControllerServlet extends HttpServlet {

    private static final Logger LOGGER = LogManager.getLogger(ControllerServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ActionFactory client = new ActionFactory();
        Command command = client.defineCommand(req);

        Router router = command.execute(req);

        switch (router.getRoute()) {
            case FORWARD:
                RequestDispatcher dispatcher =
                        getServletContext().getRequestDispatcher(router.getPagePath());
                dispatcher.forward(req, resp);
                break;
            case REDIRECT:
                resp.sendRedirect(req.getContextPath() + router.getPagePath());
                break;
        }
    }

    @Override
    public void destroy() {
        ConnectionPool.getInstance().dispose();
    }
}
