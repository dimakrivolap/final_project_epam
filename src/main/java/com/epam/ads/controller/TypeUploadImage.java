package com.epam.ads.controller;
/**
 * TypeUploadImage.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 */
public enum TypeUploadImage {
    USER, POST
}
