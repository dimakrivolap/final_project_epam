package com.epam.ads.controller;


import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;


@WebServlet("/upload")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024
        , maxFileSize = 1024 * 1024 * 5
        , maxRequestSize = 1024 * 1024 * 5 * 5)
/**
 * UploadServlet. Uploader images.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see HttpServlet
 */
public class UploadServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(UploadServlet.class);

    private static final String IMAGE_PATH = ConfigurationManager.getProperty("path.images");

    static {
        File fileSaveDir = new File(IMAGE_PATH);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        Router router = new Router();
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        String fullPath = null;
        Locale locale = (Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        HttpSession session = request.getSession();

        User user = (User) request.getSession().getAttribute("user");
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
        TypeUploadImage typeUploadImage = TypeUploadImage.valueOf(request.getParameter("typeUploadImage").toUpperCase());
        String fileName = null;

        switch (typeUploadImage) {
            case USER:
                fileName = String.valueOf(user.getId()) + ".jpg";
                fullPath = IMAGE_PATH + File.separator + "user" + File.separator + fileName;
                break;
            case POST:
                try {
                    int postId = Integer.parseInt(request.getParameter("postId"));
                    PostService postService = new PostService();
                    if (!TypeUser.ADMIN.equals(role)) {
                        if (!user.getId().equals(postService.getPost(postId).getIdUser())) {
                            LOGGER.warn("Access is denied for user:" + user.getId() + "for post:" + postId);
                            return;
                        }
                    }
                    fileName = String.valueOf(postId) + ".jpg";
                    fullPath = IMAGE_PATH + File.separator + "post" + File.separator + fileName;
                } catch (ProjectException e) {
                    LOGGER.warn("Upload image post error for user:" + user.getId());
                }
                break;
            default:

        }
        if (fullPath == null) {
            LOGGER.error("type Image error:" + typeUploadImage);
            request.setAttribute(MESSAGE_RESULT_SENDING,
                    MessageManager.valueOf(language).getMessage(MESSAGE_UPLOAD_ERROR));
            return;
        }
        for (Part part : request.getParts()) {
            if (part.getSubmittedFileName() != null) {
                part.write(fullPath);
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
                request.setAttribute(MESSAGE_RESULT_SENDING, fileName +
                        MessageManager.valueOf(language).getMessage(MESSAGE_UPLOAD_SUCCESS));
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(router.getPagePath());
                dispatcher.forward(request, response);
            }
        }
    }

}