package com.epam.ads.controller;

import com.epam.ads.resource.ConfigurationManager;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.Files;


@WebServlet("/image/*")
/**
 * ImageServlet. Getter images.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see HttpServlet
 */
public class ImageServlet extends HttpServlet {
    /**
     * The unique serial version identifier.
     */
    private static final long serialVersionUID = 1L;

    /**
     * String which contains the path to the stored images
     */
    private static final String IMAGE_PATH = ConfigurationManager.getProperty("path.images");
    private static final String DEFAULT_AVATAR = "default.jpg";
    private static final String DEFAULT_POST_IMAGE = "default_post_image.jpg";
    private static final String CONTENT_TYPE = "image/image";
    private static final String CONTENT_LENGTH = "Content-Length";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String path = request.getPathInfo() + ".jpg";
        if (path == null) {
            return;
        }
        File image = new File(IMAGE_PATH, URLDecoder.decode(path, "UTF-8"));
        if (!image.exists()) {
            if (path.contains("/user")) {
                image = new File(IMAGE_PATH, DEFAULT_AVATAR);
            } else if (path.contains("/post")) {
                image = new File(IMAGE_PATH, DEFAULT_POST_IMAGE);
            }
        }

        response.reset();
        response.setContentType(CONTENT_TYPE);
        response.setHeader(CONTENT_LENGTH, String.valueOf(image.length()));
        Files.copy(image.toPath(), response.getOutputStream());
    }
}
