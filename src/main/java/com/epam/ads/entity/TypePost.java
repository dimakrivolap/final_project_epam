package com.epam.ads.entity;
/**
 * TypePost.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Post
 */
public enum TypePost {
    BUY, SELL, RENT, EXCHANGE
}
