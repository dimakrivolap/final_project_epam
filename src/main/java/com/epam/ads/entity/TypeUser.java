package com.epam.ads.entity;
/**
 * TypeUser.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see User
 */
public enum TypeUser {
    GUEST, CLIENT, ADMIN;

    TypeUser typeUser;

    public TypeUser getRole() {
        return typeUser;
    }
}
