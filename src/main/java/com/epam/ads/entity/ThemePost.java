package com.epam.ads.entity;
/**
 * ThemePost.Entity class.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Post
 */
public class ThemePost {
    private long id;
    private String name;

    public ThemePost() {
    }

    public ThemePost(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ThemePost{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
