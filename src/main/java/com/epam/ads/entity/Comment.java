package com.epam.ads.entity;

import com.epam.ads.dao.AbstractDao;

import java.time.LocalDateTime;
/**
 * Comment.Entity class.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Entity
 */
public class Comment extends Entity {
    private Integer id;
    private Integer idUser;
    private Integer idPost;
    private String text;
    private LocalDateTime lastUpdate;

    public Comment() {
    }

    public Comment(Integer id, Integer idUser, Integer idPost, String text, LocalDateTime lastUpdate) {
        this.id = id;
        this.idUser = idUser;
        this.idPost = idPost;
        this.text = text;
        this.lastUpdate = lastUpdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdPost() {
        return idPost;
    }

    public void setIdPost(Integer idPost) {
        this.idPost = idPost;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", idUser=" + idUser +
                ", idPost=" + idPost +
                ", text='" + text + '\'' +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
