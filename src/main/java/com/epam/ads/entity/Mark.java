package com.epam.ads.entity;
/**
 * Mark.Entity class.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Entity
 */
public class Mark extends Entity {
    private Integer id;
    private Integer idUserSender;
    private Integer idUserReceiver;
    private Integer markValue;

    public Mark() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUserSender() {
        return idUserSender;
    }

    public void setIdUserSender(Integer idUserSender) {
        this.idUserSender = idUserSender;
    }

    public Integer getIdUserReceiver() {
        return idUserReceiver;
    }

    public void setIdUserReceiver(Integer idUserReceiver) {
        this.idUserReceiver = idUserReceiver;
    }

    public Integer getMarkValue() {
        return markValue;
    }

    public void setMarkValue(Integer markValue) {
        this.markValue = markValue;
    }
}
