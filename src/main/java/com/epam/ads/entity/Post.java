package com.epam.ads.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Post.Entity class.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Entity
 */
public class Post extends Entity {
    private Integer id;
    private Integer idThemePost;
    private Integer idUser;
    private TypePost typePost;
    private String miniDescription;
    private String context;
    private BigDecimal price;
    private LocalDateTime dateCreated;
    private LocalDateTime lastUpdate;

    public Post() {
    }


    public Post(Integer id, Integer idThemePost, TypePost typePost, String miniDescription, String context, BigDecimal price) {
        this.id = id;
        this.idThemePost = idThemePost;
        this.typePost = typePost;
        this.miniDescription = miniDescription;
        this.context = context;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdThemePost() {
        return idThemePost;
    }

    public void setIdThemePost(Integer idThemePost) {
        this.idThemePost = idThemePost;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public TypePost getTypePost() {
        return typePost;
    }

    public void setTypePost(TypePost typePost) {
        this.typePost = typePost;
    }

    public String getMiniDescription() {
        return miniDescription;
    }

    public void setMiniDescription(String miniDescription) {
        this.miniDescription = miniDescription;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", themePostId=" + idThemePost +
                ", idUser=" + idUser +
                ", typePost=" + typePost +
                ", miniDescription='" + miniDescription + '\'' +
                ", context='" + context + '\'' +
                ", price=" + price +
                ", dateCreated=" + dateCreated +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
