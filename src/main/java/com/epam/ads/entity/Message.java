package com.epam.ads.entity;

import java.time.LocalDateTime;
/**
 * Message.Entity class.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Entity
 */
public class Message extends Entity {
    private Integer id;
    private Integer idUserSender;
    private Integer idUserReceiver;
    private String text;
    private LocalDateTime dateCreated;

    public Message() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdUserSender() {
        return idUserSender;
    }

    public void setIdUserSender(Integer idUserSender) {
        this.idUserSender = idUserSender;
    }

    public Integer getIdUserReceiver() {
        return idUserReceiver;
    }

    public void setIdUserReceiver(Integer idUserReceiver) {
        this.idUserReceiver = idUserReceiver;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }
}
