package com.epam.ads.command;
/**
 * Class with constants parameter,atribut,paths, messages ...
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public final class CommandConstant {
    public static final String PARAM_NAME_LOCALE_VALUE = "value";
    public static final String PARAM_NAME_VALUE = "value";
    public static final String PARAM_NAME_LOGIN = "login";
    public static final String PARAM_NAME_PASSWORD = "password";
    public static final String PARAM_NAME_OLD_PASSWORD = "old_password";
    public static final String PARAM_NAME_FIRST_NAME = "first_name";
    public static final String PARAM_NAME_LAST_NAME = "last_name";
    public static final String PARAM_NAME_EMAIL = "email";
    public static final String PARAM_NAME_PHONE_NUMBER = "phone_number";
    public static final String PARAM_NAME_THEME_POST = "theme_post";
    public static final String PARAM_NAME_TYPE_POST = "type_post";
    public static final String PARAM_NAME_TYPE_MESSAGES = "type_messages";
    public static final String PARAM_NAME_MINI_DESCRIPTION = "mini_description";
    public static final String PARAM_NAME_PRICE = "price";
    public static final String PARAM_NAME_CONTEXT = "context";
    public static final String PARAM_COMMAND = "command";
    public static final String PARAM_LOGIN_RECEIVER = "receiver";
    public static final String PARAM_ID_POST = "id";
    public static final String PARAM_ID_USER = "id";
    public static final String PARAM_NAME_ID_COMMENT = "id_comment";
    public static final String PARAM_NAME_TEXT = "text";
    public static final String PARAM_NAME_ID_POST = "id_post";
    public static final String PARAM_NAME_ID_USER = "id_user";
    public static final String PARAM_AVAILABLE = "available";
    public static final String PARAM_ID_RECEIVER = "idReceiver";
    public static final String PARAM_SEARCH_PHRASE = "search_phrase";
    public static final String PARAM_PRICE_MIN = "price_min";
    public static final String PARAM_PRICE_MAX = "price_max";
    public static final String PARAM_START = "start";
    public static final String PARAM_THEME = "theme";



    public static final String TYPE_MESSAGES_RECEIVED = "received";
    public static final String TYPE_MESSAGES_SENT = "sent";





    public static final String PATH_PAGE_CLIENT_MAIN = "path.page.client.main";
    public static final String PATH_PAGE_LOGIN = "path.page.login";
    public static final String PATH_PAGE_REGISTRATION = "path.page.registration";
    public static final String PATH_PAGE_MAIN = "path.page.main";
    public static final String PATH_PAGE_USER = "path.page.user";
    public static final String PATH_PAGE_USER_POSTS = "path.page.user.posts";
    public static final String PATH_PAGE_USER_POSTS_COMMAND = "/controller?command=user_posts";
    public static final String PATH_PAGE_THEMES_POSTS = "path.page.themes.posts";
    public static final String PATH_PAGE_POSTS = "path.page.posts";
    public static final String PATH_PAGE_POST = "path.page.post";
    public static final String PATH_PAGE_CLIENT_NEW_POST = "path.page.client.new_post";
    public static final String PATH_PAGE_ADMIN_NEW_POST = "path.page.admin.new_post";
    public static final String PATH_PAGE_CLIENT_MODIFY_POST = "path.page.client.modify_post";
    public static final String PATH_PAGE_INDEX= "path.page.index";
    public static final String PATH_PAGE_PROFILE= "path.page.profile";
    public static final String PATH_PAGE_DEFAULT= "path.page.default";
    public static final String PATH_PAGE_ABOUT= "path.page.about";
    public static final String PATH_PAGE_SEARCH= "path.page.search";
    public static final String PATH_PAGE_ADMIN_SEARCH= "path.page.admin.search";
    public static final String PATH_PAGE_MESSAGES= "path.page.messages";
    public static final String PATH_PAGE_SEND_MESSAGE= "path.page.message.send";



    public static final String ATTRIBUTE_LOCALE = "locale";
    public static final String ATTRIBUTE_USER = "user";
    public static final String ATTRIBUTE_RATING = "rating";
    public static final String ATTRIBUTE_USER_SENDER = "sender";
    public static final String ATTRIBUTE_USER_RECEIVER = "receiver";
    public static final String ATTRIBUTE_ROLE = "role";
    public static final String ATTRIBUTE_POSTS = "posts";
    public static final String ATTRIBUTE_POST = "post";
    public static final String ATTRIBUTE_THEME = "theme";
    public static final String ATTRIBUTE_COUNT_PAGES = "count_pages";
    public static final String ATTRIBUTE_CURRENT_PAGE = "current_page";
    public static final String ATTRIBUTE_START = "start";
    public static final String ATTRIBUTE_USER_OWNER = "user_owner";
    public static final String ATTRIBUTE_USERS = "users";
    public static final String ATTRIBUTE_MESSAGES = "messages";
    public static final String ATTRIBUTE_MARK = "mark";
    public static final String ATTRIBUTE_TYPE_MESSAGE = "type_messages";
    public static final String ATTRIBUTE_COMMENTS = "comments";
    public static final String ATTRIBUTE_COMMENTATORS = "commentators";
    public static final String ATTRIBUTE_URL_QUERY = "urlQuery";

    public static final String ATTRIBUTE_THEMES = "themes";
    public static final String ATTRIBUTE_URL = "url";
    public static final String ATTRIBUTE_SEARCH = "search";
    public static final String ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD = "errorLoginPassMessage";
    public static final String ATTRIBUTE_ERROR_REGISTRATION_DATA = "errorRegistrationMessage";
    public static final String MESSAGE_RESULT_CREATION_POST = "resultCreationPost";
    public static final String MESSAGE_RESULT_SENDING = "result";
    public static final String ATRIBUTE_WRONG_ACTION = "wrongAction";

    public static final String MESSAGE_VALIDATION_ERROR = "message.validationError";
    public static final String MESSAGE_CREATE_POST_SUCCESS = "message.successCreationPost";
    public static final String MESSAGE_SENT_SUCCESS = "message.successSentMessage";
    public static final String MESSAGE_SENT_ERROR = "message.errorSentMessage";
    public static final String MESSAGE_UPDATE_POST_SUCCESS = "message.successUpdatePost";
    public static final String MESSAGE_UPLOAD_SUCCESS = "message.uploadSuccessful";
    public static final String MESSAGE_UPLOAD_ERROR = "message.uploadError";
    public static final String MESSAGE_CREATE_POST_ERROR = "message.errorCreationPost";
    public static final String MESSAGE_UPDATE_POST_ERROR = "message.errorUpdatePost";

    public static final String MESSAGE_LOGIN_ERROR = "message.loginError";
    public static final String MESSAGE_WRONG_ACTION = "message.wrongAction";
    public static final String MESSAGE_REGISTRATION_ERROR = "message.registrationError";
    public static final String MESSAGE_CREATE_COMMENT_ERROR = "message.errorCreationComment";
    public static final String MESSAGE_LOCKED_ERROR = "message.errorLocked";


    public static final int POSTS_ON_PAGE = 5;
    public static final int MESSAGES_ON_PAGE = 5;


    public static final String RETRIEVAL_SUBJECT = "Retrieval password";


}
