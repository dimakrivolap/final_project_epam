package com.epam.ads.command;


import com.epam.ads.controller.Router;

import javax.servlet.http.HttpServletRequest;

@FunctionalInterface
public interface Command {
    Router execute(HttpServletRequest request);
}
