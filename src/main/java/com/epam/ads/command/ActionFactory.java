package com.epam.ads.command;


import com.epam.ads.command.impl.EmptyCommand;
import com.epam.ads.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

import static com.epam.ads.command.CommandConstant.*;
/**
 * Factory Commands Class.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class ActionFactory {

    public static Optional<Command> getCommand(String commandName) {
        return Arrays.stream(CommandType.values())
                .filter(o -> o.name().equalsIgnoreCase(commandName))
                .map(CommandType::getCommand)
                .findAny();
        //Optional.ofNullable(type.orElse(null).getCommand());
    }

    public Command defineCommand(HttpServletRequest request) { //optional
        Command current = new EmptyCommand();
        String action = request.getParameter(PARAM_COMMAND);
        if (action == null || action.isEmpty()) {
            return current;
        }
        try {
            CommandType currentType = CommandType.valueOf(action.toUpperCase());
            current = currentType.getCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute(ATRIBUTE_WRONG_ACTION, action
                    + MessageManager.valueOf((String) request.getAttribute(ATTRIBUTE_LOCALE)).getMessage(MESSAGE_WRONG_ACTION));
        }
        return current;
    }
}
