package com.epam.ads.command;

import java.util.Arrays;
import java.util.stream.Stream;
/**
 * Creates instances which implements {@link Command}.
 * Utility class, therefore final as it's not designed for instantiation and
 * extension.
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class CommandFactory {
    private static CommandFactory instance;

    private CommandFactory() {
    }

    public static CommandFactory getInstance() {
        if (instance == null) {
            instance = new CommandFactory();
        }
        return instance;
    }

    public static Stream<CommandType> defineCommand(String commandName) {
        return Arrays.stream(CommandType.values());
    }


}
