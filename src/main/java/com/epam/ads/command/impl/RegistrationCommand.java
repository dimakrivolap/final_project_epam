package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.RegistrationService;
import com.epam.ads.validation.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class RegistrationCommand implements Command {

    private RegistrationService registrationService;

    public RegistrationCommand(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();

        String login = request.getParameter(PARAM_NAME_LOGIN).replaceAll("</?script>", "");
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        String firstName = request.getParameter(PARAM_NAME_FIRST_NAME).replaceAll("</?script>", "");
        String lastName = request.getParameter(PARAM_NAME_LAST_NAME).replaceAll("</?script>", "");
        String email = request.getParameter(PARAM_NAME_EMAIL).replaceAll("</?script>", "");
        String phoneNumber = request.getParameter(PARAM_NAME_PHONE_NUMBER).replaceAll("</?script>", "");

        User user = new User(login, password, firstName, lastName, email, phoneNumber);
        HttpSession session = request.getSession();
        Locale locale = ((Locale) session.getAttribute(ATTRIBUTE_LOCALE));
        String language = locale.getLanguage().toUpperCase();
        if (Validator.checkUserRegistration(user)) {
            try {

                registrationService.register(user);
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));

            } catch (ProjectException e) {
                request.setAttribute(ATTRIBUTE_ERROR_REGISTRATION_DATA, MessageManager.valueOf(language).getMessage(MESSAGE_REGISTRATION_ERROR));
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_REGISTRATION));
            }
        } else {
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD, MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_REGISTRATION));
        }
        return router;
    }

}
