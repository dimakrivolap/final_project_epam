package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
import static com.epam.ads.command.CommandConstant.PATH_PAGE_POSTS;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class FindPostCommand implements Command {
    private PostService postService;
    public FindPostCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Locale locale = (Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        try {
        //int themeId = Integer.parseInt(request.getParameter("theme"));
        String searchPhrase = request.getParameter(PARAM_SEARCH_PHRASE);
        String typePost = request.getParameter(PARAM_NAME_TYPE_POST);
        String priceMinParam = request.getParameter(PARAM_PRICE_MIN);
        String priceMaxParam = request.getParameter(PARAM_PRICE_MAX);
        BigDecimal priceMin;
        BigDecimal priceMax;
        if(!priceMinParam.isEmpty()) {
            priceMin = new BigDecimal(priceMinParam);
        }
        else {
            priceMin = new BigDecimal(0);
        }
        if(!priceMaxParam.isEmpty()){
            priceMax = new BigDecimal(priceMaxParam);
        }
        else {
            priceMax = new BigDecimal(99999999.99);
        }
        int start = Integer.parseInt(request.getParameter(PARAM_START));


            List<Post> posts = postService.getPosts(searchPhrase,typePost,priceMin,priceMax, start);

            if(posts.size()>=POSTS_ON_PAGE+start) {
                posts = posts.subList(start, start + POSTS_ON_PAGE);
            }
            else {
                posts =posts.subList(start,posts.size());
            }

            int countPages =posts.size()/POSTS_ON_PAGE;
            int currentPage = start / POSTS_ON_PAGE;
            request.setAttribute(ATTRIBUTE_POSTS, posts);
            request.setAttribute(ATTRIBUTE_CURRENT_PAGE, currentPage);
            request.setAttribute(ATTRIBUTE_COUNT_PAGES, countPages);
            String url = (String)request.getAttribute(ATTRIBUTE_URL_QUERY);
            url = url.replaceAll("&*start=[\\d+]","");
            request.setAttribute(ATTRIBUTE_URL, url);
            request.setAttribute(ATTRIBUTE_SEARCH, true);
            request.setAttribute(ATTRIBUTE_START, start);
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_POSTS));
        } catch (ProjectException|NumberFormatException e) {
            request.setAttribute(ATRIBUTE_WRONG_ACTION,
                    MessageManager.valueOf(language).getMessage(MESSAGE_WRONG_ACTION));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }


        return router;
    }

}
