package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class PostsCommand implements Command {
    private PostService postService;

    public PostsCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Locale locale = (Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();

        int themeId = Integer.parseInt(request.getParameter(PARAM_THEME));
        int start = Integer.parseInt(request.getParameter(PARAM_START));
        try {
            List<Post> posts = postService.getPosts(themeId, start);
            ThemePost theme = postService.getThemeById(themeId);
            int countPages = postService.getCountPostsByThemeId(themeId) / POSTS_ON_PAGE;
            int currentPage = start / POSTS_ON_PAGE;
            request.setAttribute(ATTRIBUTE_POSTS, posts);
            request.setAttribute(ATTRIBUTE_THEME, theme);
            request.setAttribute(ATTRIBUTE_COUNT_PAGES, countPages);
            request.setAttribute(ATTRIBUTE_CURRENT_PAGE, currentPage);
            request.setAttribute(ATTRIBUTE_START, start);
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_POSTS));

        } catch (ProjectException e) {
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }

        return router;
    }

}
