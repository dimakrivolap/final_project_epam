package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class NewPostCommand implements Command {
    private PostService postService;

    public NewPostCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String page;
        HttpSession session = request.getSession();
        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        try {
            List<ThemePost> themes = postService.getAllThemes(locale);
            request.setAttribute(ATTRIBUTE_THEMES, themes);

        } catch (ProjectException e) {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
        switch (role) {
            case CLIENT:
            case ADMIN:
                page = ConfigurationManager.getProperty(PATH_PAGE_CLIENT_NEW_POST);
                break;
            case GUEST:
            default:
                page = ConfigurationManager.getProperty(PATH_PAGE_MAIN);
        }
        router.setPagePath(page);
        router.setRoute(Router.RouteType.FORWARD);
        return router;
    }
}
