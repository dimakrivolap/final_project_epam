package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.ads.command.CommandConstant.*;

/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class ChangeAvailableCommand implements Command {
    private UserService userService;

    public ChangeAvailableCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        try {
            String availableParam = request.getParameter(PARAM_AVAILABLE);
            boolean available = Boolean.parseBoolean(availableParam);
            int idUser = Integer.parseInt(request.getParameter(PARAM_NAME_ID_USER));
            User user = (User) session.getAttribute(ATTRIBUTE_USER);
            TypeUser role = user.getTypeUser();
            if (TypeUser.ADMIN.equals(role)) {
                userService.updateAvailable(idUser, available);
            }
        } catch (ProjectException e) {

        }
        return router;
    }

}
