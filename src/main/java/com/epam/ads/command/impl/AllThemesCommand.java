package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;

/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class AllThemesCommand implements Command {
    private PostService postService;

    public AllThemesCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Locale locale = (Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        try {
            List<ThemePost> themes = postService.getAllThemes(locale);
            request.setAttribute(ATTRIBUTE_THEMES, themes);
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_THEMES_POSTS));
        } catch (ProjectException e) {
            request.setAttribute(ATRIBUTE_WRONG_ACTION,
                    MessageManager.valueOf(language).getMessage(MESSAGE_WRONG_ACTION));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }

}
