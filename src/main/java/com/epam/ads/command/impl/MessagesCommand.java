package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Message;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.MessageService;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class MessagesCommand implements Command {
    private MessageService messageService;
    private UserService userService;

    public MessagesCommand(MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        String typeMessages = request.getParameter(PARAM_NAME_TYPE_MESSAGES);
        if (typeMessages == null) {
            typeMessages = TYPE_MESSAGES_RECEIVED;
        }
        if (user != null) {
            int start = Integer.parseInt(request.getParameter(PARAM_START));
            int countPages = 0;
            request.setAttribute(ATTRIBUTE_TYPE_MESSAGE, typeMessages);
            try {
                List<Message> messages;
                if (TYPE_MESSAGES_SENT.equals(typeMessages)) {
                    messages = messageService.getSentMessages(user.getId(), start);
                    countPages = messageService.getCountSentMessages(user.getId()) / MESSAGES_ON_PAGE;

                } else {
                    messages = messageService.getReseivedMessages(user.getId(), start);
                    countPages = messageService.getCountReceivedMessages(user.getId()) / MESSAGES_ON_PAGE;
                }
                List<User> users = new ArrayList<>();
                for (Message message : messages) {
                    User userSender = userService.findUser(message.getIdUserSender());
                    users.add(userSender);
                }

                int currentPage = start / MESSAGES_ON_PAGE;
                request.setAttribute(ATTRIBUTE_MESSAGES, messages);
                request.setAttribute(ATTRIBUTE_USERS, users);
                request.setAttribute(ATTRIBUTE_COUNT_PAGES, countPages);
                request.setAttribute(ATTRIBUTE_CURRENT_PAGE, currentPage);

            } catch (ProjectException e) {
                request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                        MessageManager.valueOf(locale.getLanguage()).getMessage(MESSAGE_LOGIN_ERROR));
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            }

            request.setAttribute(ATTRIBUTE_START, start);
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MESSAGES));
        } else {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }

}
