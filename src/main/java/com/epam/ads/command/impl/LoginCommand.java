package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.MarkService;
import com.epam.ads.service.UserService;
import com.epam.ads.validation.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class LoginCommand implements Command {

    private UserService userService;
    private MarkService markService;

    public LoginCommand(UserService userService, MarkService markService) {
        this.userService = userService;
        this.markService = markService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();

        String login = request.getParameter(PARAM_NAME_LOGIN);
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        HttpSession session = request.getSession();
        Locale locale = ((Locale) session.getAttribute(ATTRIBUTE_LOCALE));
        User user;
        String language = locale.getLanguage().toUpperCase();
        if (Validator.checkLogin(login) && Validator.checkPassword(password)) {
            try {
                user = userService.checkUser(login, password);
                if (user != null) {
                    session.setAttribute(ATTRIBUTE_USER, user);
                    session.setAttribute(ATTRIBUTE_ROLE, user.getTypeUser());
                    DecimalFormat formatter = new DecimalFormat("#0.00");
                    double rating = markService.getRating(user.getId());
                    session.setAttribute(ATTRIBUTE_RATING, formatter.format(rating));
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_CLIENT_MAIN));
                    router.setRoute(Router.RouteType.REDIRECT);
                } else {
                    request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                            MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_LOGIN));
                }

            } catch (ProjectException e) {
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
                request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                        MessageManager.valueOf(language).getMessage(MESSAGE_VALIDATION_ERROR));
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            }
        } else {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(language).getMessage(MESSAGE_VALIDATION_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }

        return router;
    }

}
