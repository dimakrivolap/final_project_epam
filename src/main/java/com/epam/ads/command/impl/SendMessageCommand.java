package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.MessageService;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class SendMessageCommand implements Command {
    private MessageService messageService;
    private UserService userService;

    public SendMessageCommand(MessageService messageService, UserService userService) {
        this.messageService = messageService;
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        String loginReceiver = request.getParameter(PARAM_LOGIN_RECEIVER);
        User sessionUser = (User) session.getAttribute(ATTRIBUTE_USER);
        String message = request.getParameter(PARAM_NAME_TEXT).replaceAll("</?script>", "");
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        Locale locale = ((Locale) session.getAttribute(ATTRIBUTE_LOCALE));
        String language = locale.getLanguage().toUpperCase();
        if (sessionUser.isAvailable()) {
            try {

                if (sessionUser != null) {
                    User receiver = userService.findUser(loginReceiver);
                    messageService.createMessage(sessionUser.getId(), receiver.getId(), message);
                    request.setAttribute(MESSAGE_RESULT_SENDING,
                            MessageManager.valueOf(language).getMessage(MESSAGE_SENT_SUCCESS));

                }
            } catch (ProjectException e) {
                request.setAttribute(MESSAGE_RESULT_SENDING,
                        MessageManager.valueOf(language).getMessage(MESSAGE_SENT_ERROR));
            }
        } else {
            request.setAttribute(MESSAGE_WRONG_ACTION,
                    MessageManager.valueOf(language).getMessage(MESSAGE_LOCKED_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }

}
