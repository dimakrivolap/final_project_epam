package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class UserPostsCommand implements Command {
    private PostService postService;

    public UserPostsCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Locale locale = (Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        User user = (User) request.getSession().getAttribute(ATTRIBUTE_USER);
        try {
            String login = user.getLogin();
            List<Post> posts = postService.getPosts(login);
            request.setAttribute(ATTRIBUTE_POSTS, posts);
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_USER_POSTS));
        } catch (ProjectException e) {
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }

        return router;
    }

}
