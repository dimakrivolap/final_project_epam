package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;

/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class ModifyPostCommand implements Command {
    private PostService postService;

    public ModifyPostCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String page;
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        Integer postId = Integer.parseInt(request.getParameter(PARAM_ID_POST));
        if(user.isAvailable()) {
            try {
                Post post = postService.getPost(postId);
                request.setAttribute(ATTRIBUTE_POST, post);
                List<ThemePost> themes = postService.getAllThemes(locale);
                request.setAttribute(ATTRIBUTE_THEMES, themes);
            } catch (ProjectException e) {
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            }
            TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
            switch (role) {
                case CLIENT:
                case ADMIN:
                    page = ConfigurationManager.getProperty(PATH_PAGE_CLIENT_MODIFY_POST);
                    break;
                case GUEST:
                default:
                    page = ConfigurationManager.getProperty(PATH_PAGE_MAIN);
            }
            router.setPagePath(page);
        }
        else {
            request.setAttribute(MESSAGE_WRONG_ACTION,
                    MessageManager.valueOf(language).getMessage(MESSAGE_LOCKED_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        router.setRoute(Router.RouteType.FORWARD);
        return router;
    }

}
