package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class SearchPageCommand implements Command {
    private PostService postService;

    public SearchPageCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String page;
        HttpSession session = request.getSession();
        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        try {

            List<ThemePost> themes = postService.getAllThemes(locale);
            request.setAttribute(ATTRIBUTE_THEMES, themes);
        } catch (ProjectException e) {

        }
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
        if (TypeUser.ADMIN.equals(role)) {
            page = ConfigurationManager.getProperty(PATH_PAGE_ADMIN_SEARCH);
        } else {
            page = ConfigurationManager.getProperty(PATH_PAGE_SEARCH);
        }
        router.setPagePath(page);
        return router;
    }

}
