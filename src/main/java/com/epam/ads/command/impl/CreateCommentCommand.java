package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Comment;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.CommentService;
import com.epam.ads.service.PostService;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class CreateCommentCommand implements Command {
    private CommentService commentService;
    private PostService postService;
    private UserService userService;

    public CreateCommentCommand(CommentService commentService, PostService postService, UserService userService) {
        this.commentService = commentService;
        this.postService = postService;
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        String text = request.getParameter(PARAM_NAME_TEXT).replaceAll("</?script>", "");
        Integer idPost = Integer.parseInt(request.getParameter(PARAM_NAME_ID_POST));
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        Integer idUser = user.getId();

        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_POST));

        if (TypeUser.CLIENT.equals(role) || TypeUser.ADMIN.equals(role)) {
            if (user.isAvailable()) {
                Comment comment = new Comment();
                comment.setIdUser(idUser);
                comment.setIdPost(idPost);
                comment.setText(text);
                try {
                    commentService.createComment(comment);

                    Post post = postService.getPost(idPost);
                    User userOwner = userService.findUser(post.getIdUser());
                    List<Comment> comments = commentService.getCommentsByPostId(idPost);
                    List<User> commentators = new ArrayList<>();
                    for (Comment c : comments) {
                        User userCommentator = userService.findUser(c.getIdUser());
                        commentators.add(userCommentator);
                    }
                    request.setAttribute(ATTRIBUTE_POST, post);
                    request.setAttribute(ATTRIBUTE_USER_OWNER, userOwner);
                    request.setAttribute(ATTRIBUTE_COMMENTS, comments);
                    request.setAttribute(ATTRIBUTE_COMMENTATORS, commentators);

                } catch (ProjectException e) {
                    request.setAttribute(MESSAGE_WRONG_ACTION,
                            MessageManager.valueOf(language).getMessage(MESSAGE_CREATE_COMMENT_ERROR));
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
                }
            } else {
                request.setAttribute(MESSAGE_WRONG_ACTION,
                        MessageManager.valueOf(language).getMessage(MESSAGE_LOCKED_ERROR));
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            }
        }
        return router;
    }


}
