package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.MarkService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class ChangeMarkCommand implements Command {
    private MarkService markService;

    public ChangeMarkCommand(MarkService markService) {
        this.markService = markService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String language = request.getParameter(PARAM_NAME_VALUE);
        HttpSession session = request.getSession();
        User user = (User) (session.getAttribute(ATTRIBUTE_USER));
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        int idUserReceiver = Integer.parseInt(request.getParameter(PARAM_ID_RECEIVER));
        int valueMark = Integer.parseInt(request.getParameter(PARAM_NAME_VALUE));
        try {
            if ((user != null) && (user.getId() != idUserReceiver)) {
                markService.changeMark(user.getId(), idUserReceiver, valueMark);
                router.setRoute(Router.RouteType.FORWARD);
            }
        } catch (ProjectException e) {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }

}
