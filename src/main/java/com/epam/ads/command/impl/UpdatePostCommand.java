package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.TypePost;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class UpdatePostCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UpdatePostCommand.class);
    private PostService postService;

    public UpdatePostCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Integer postId = Integer.parseInt(request.getParameter(PARAM_ID_POST));
        String themePostParameter = request.getParameter(PARAM_NAME_THEME_POST);
        Integer idThemePost = Integer.parseInt(themePostParameter);
        String typePostParameter = request.getParameter(PARAM_NAME_TYPE_POST).toUpperCase();
        TypePost typePost = TypePost.valueOf(typePostParameter);
        String miniDescription = request.getParameter(PARAM_NAME_MINI_DESCRIPTION).replaceAll("</?script>", "");
        String priceParameter = request.getParameter(PARAM_NAME_PRICE).replaceAll(",", "");
        BigDecimal price = new BigDecimal(priceParameter);
        String context = request.getParameter(PARAM_NAME_CONTEXT);

        HttpSession session = request.getSession();
        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));

        Post post = new Post();
        post.setIdThemePost(idThemePost);
        post.setId(postId);
        post.setTypePost(typePost);
        post.setMiniDescription(miniDescription);
        post.setContext(context);
        post.setPrice(price);

        if (TypeUser.CLIENT.equals(role)) {
            router.setPagePath(PATH_PAGE_USER_POSTS_COMMAND);
            try {
                List<Post> posts = postService.getPosts(user.getLogin());
                for (Post p : posts) {
                    if (p.getId().equals(postId)) {
                        postService.updatePost(post);
                        break;
                    }
                }

            } catch (ProjectException e) {

            }
        } else if (TypeUser.ADMIN.equals(role)) {
            try {
                postService.updatePost(post);

            } catch (ProjectException e) {

            }
        }

        return router;
    }

}
