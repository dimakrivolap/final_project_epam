package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.MailService;
import com.epam.ads.service.UserService;
import org.apache.commons.lang3.RandomStringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.ResourceBundle;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class PasswordRetrievalCommand implements Command {
    private static ResourceBundle mailResources = ResourceBundle.getBundle("mail");
    private UserService userService;

    public PasswordRetrievalCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Locale locale = ((Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE));
        String language = locale.getLanguage().toUpperCase();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        String newPassword = RandomStringUtils.random(30, characters);
        String toEmail = request.getParameter(PARAM_NAME_EMAIL);
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        try {
            userService.updatePassword(toEmail, newPassword);
            String mailText = "New Password:" + newPassword;
            new MailService(
                    toEmail,
                    RETRIEVAL_SUBJECT,
                    mailText,
                    mailResources).start();
            request.setAttribute(MESSAGE_RESULT_SENDING,
                    MessageManager.valueOf(language).getMessage(MESSAGE_SENT_SUCCESS));
        } catch (ProjectException e) {
            //LOGGER
            request.setAttribute(MESSAGE_RESULT_SENDING,
                    MessageManager.valueOf(language).getMessage(MESSAGE_SENT_ERROR));
        }
        return router;
    }

}
