package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class SendMessagePageCommand implements Command {

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        String loginReceiver = request.getParameter(PARAM_LOGIN_RECEIVER);
        User sessionUser = (User) session.getAttribute(ATTRIBUTE_USER);

        if (sessionUser != null) {
            //String loginSender = sessionUser.getLogin();
            request.setAttribute(ATTRIBUTE_USER_RECEIVER, loginReceiver);
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_SEND_MESSAGE));
        } else {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;

    }

}
