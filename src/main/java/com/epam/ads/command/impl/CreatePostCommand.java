package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.TypePost;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.PostService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class CreatePostCommand implements Command {
    private PostService postService;

    public CreatePostCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        String themePostParameter = request.getParameter(PARAM_NAME_THEME_POST);
        Integer idThemePost = Integer.parseInt(themePostParameter);
        String typePostParameter = request.getParameter(PARAM_NAME_TYPE_POST).toUpperCase();
        TypePost typePost = TypePost.valueOf(typePostParameter);
        String miniDescription = request.getParameter(PARAM_NAME_MINI_DESCRIPTION).replaceAll("</?script>", "");
        if (miniDescription.length() > 255) {
            return router;
        }
        String priceParameter = request.getParameter(PARAM_NAME_PRICE).replaceAll(",", "");
        BigDecimal price = new BigDecimal(priceParameter);
        String context = request.getParameter(PARAM_NAME_CONTEXT);

        HttpSession session = request.getSession();
        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);

        if (TypeUser.CLIENT.equals(role) || TypeUser.ADMIN.equals(role)) {
            if (user.isAvailable()) {
                Post post = new Post();
                post.setIdThemePost(idThemePost);
                post.setIdUser(user.getId());
                post.setTypePost(typePost);
                post.setMiniDescription(miniDescription);
                post.setContext(context);
                post.setPrice(price);
                try {

                    postService.createPost(post);
                    session.setAttribute(ATTRIBUTE_USER, user);
                    session.setAttribute(MESSAGE_RESULT_CREATION_POST,
                            MessageManager.valueOf(language).getMessage(MESSAGE_CREATE_POST_SUCCESS));
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_CLIENT_MAIN));
                    router.setRoute(Router.RouteType.REDIRECT);

                } catch (ProjectException e) {
                    request.setAttribute(MESSAGE_RESULT_CREATION_POST,
                            MessageManager.valueOf(language).getMessage(MESSAGE_CREATE_POST_ERROR));
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
                }
            } else {
                request.setAttribute(MESSAGE_WRONG_ACTION,
                        MessageManager.valueOf(language).getMessage(MESSAGE_LOCKED_ERROR));
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            }
        }
        return router;
    }

}