package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Comment;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.CommentService;
import com.epam.ads.service.PostService;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class DeleteCommentCommand implements Command {
    private CommentService commentService;
    private PostService postService;
    private UserService userService;

    public DeleteCommentCommand(CommentService commentService, PostService postService, UserService userService) {
        this.commentService = commentService;
        this.postService = postService;
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        Integer idComment = Integer.parseInt(request.getParameter(PARAM_NAME_ID_COMMENT));
        Integer idPost = Integer.parseInt(request.getParameter(PARAM_NAME_ID_POST));

        Locale locale = (Locale) session.getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        TypeUser role = (TypeUser) session.getAttribute(ATTRIBUTE_ROLE);
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_POST));
        try {
            if (TypeUser.CLIENT.equals(role)) {
                List<User> commentators = new ArrayList<>();
                List<Comment> comments = commentService.getCommentsByPostId(idPost);

                for (Comment comment : comments) {
                    User userCommentator = userService.findUser(comment.getIdUser());
                    commentators.add(userCommentator);
                    if (comment.getId().equals(idComment)) {
                        commentService.deleteComment(idComment);
                        comments.remove(comment);
                        break;
                    }
                }

                Post post = postService.getPost(idPost);
                User userOwner = userService.findUser(post.getIdUser());

                request.setAttribute(ATTRIBUTE_POST, post);
                request.setAttribute(ATTRIBUTE_USER_OWNER, userOwner);
                request.setAttribute(ATTRIBUTE_COMMENTS, comments);
                request.setAttribute(ATTRIBUTE_COMMENTATORS, commentators);

            } else if (TypeUser.ADMIN.equals(role)) {
                commentService.deleteComment(idComment);
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            } else {
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            }
        } catch (ProjectException e) {
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(language).getMessage(MESSAGE_CREATE_COMMENT_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }

}
