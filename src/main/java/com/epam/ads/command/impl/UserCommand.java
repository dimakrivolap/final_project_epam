package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.MarkService;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.DecimalFormat;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class UserCommand implements Command {
    private UserService userService;
    private MarkService markService;

    public UserCommand(UserService userService, MarkService markService) {
        this.userService = userService;
        this.markService = markService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        int idUser = Integer.parseInt(request.getParameter(PARAM_ID_USER));
        User sessionUser = (User) session.getAttribute(ATTRIBUTE_USER);
        try {
            if (sessionUser != null) {
                double mark = markService.findMark(sessionUser.getId(), idUser);
                request.setAttribute(ATTRIBUTE_MARK, mark);
            }
            DecimalFormat formatter = new DecimalFormat("#0.00");
            double rating = markService.getRating(idUser);
            User user = userService.findUser(idUser);
            request.setAttribute(ATTRIBUTE_USER, user);
            request.setAttribute(ATTRIBUTE_RATING, formatter.format(rating));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_USER));
        } catch (ProjectException e) {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }


}
