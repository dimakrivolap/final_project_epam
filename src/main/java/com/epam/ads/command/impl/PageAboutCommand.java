package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

import static com.epam.ads.command.CommandConstant.PATH_PAGE_ABOUT;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class PageAboutCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String page = ConfigurationManager.getProperty(PATH_PAGE_ABOUT);
        router.setPagePath(page);
        router.setRoute(Router.RouteType.FORWARD);
        return router;
    }

}
