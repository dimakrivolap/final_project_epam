package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Comment;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.CommentService;
import com.epam.ads.service.PostService;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class PostCommand implements Command {
    private PostService postService;
    private UserService userService;
    private CommentService commentService;

    public PostCommand(PostService postService, UserService userService, CommentService commentService) {
        this.postService = postService;
        this.userService = userService;
        this.commentService = commentService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        Locale locale = (Locale) request.getSession().getAttribute(ATTRIBUTE_LOCALE);
        String language = locale.getLanguage().toUpperCase();
        int postId = Integer.parseInt(request.getParameter(PARAM_ID_POST));
        try {
            Post post = postService.getPost(postId);
            User userOwner = userService.findUser(post.getIdUser());
            List<Comment> comments = commentService.getCommentsByPostId(postId);
            List<User> commentators = new ArrayList<>();
            for (Comment comment : comments) {
                User userCommentator = userService.findUser(comment.getIdUser());
                commentators.add(userCommentator);
            }
            request.setAttribute(ATTRIBUTE_POST, post);
            request.setAttribute(ATTRIBUTE_USER_OWNER, userOwner);
            request.setAttribute(ATTRIBUTE_COMMENTS, comments);
            request.setAttribute(ATTRIBUTE_COMMENTATORS, commentators);

        } catch (ProjectException e) {
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_LOGIN));
        }
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_POST));
        return router;
    }

}
