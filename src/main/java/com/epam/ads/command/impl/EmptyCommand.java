package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.resource.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

import static com.epam.ads.command.CommandConstant.PATH_PAGE_DEFAULT;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class EmptyCommand implements Command {
    @Override
    public Router execute(HttpServletRequest request) {

        Router router = new Router();
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_DEFAULT));
        return router;
    }
}
