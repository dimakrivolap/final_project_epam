package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.service.PostService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class DeletePostCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(DeletePostCommand.class);

    private PostService postService;

    public DeletePostCommand(PostService postService) {
        this.postService = postService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        try {
            User user = (User) session.getAttribute(ATTRIBUTE_USER);
            TypeUser role = user.getTypeUser();
            Integer postId = Integer.parseInt(request.getParameter(PARAM_ID_POST));

            if (TypeUser.CLIENT.equals(role)) {
                router.setPagePath(PATH_PAGE_USER_POSTS_COMMAND);
                try {
                    List<Post> posts = postService.getPosts(user.getLogin());
                    for (Post post : posts) {
                        if (post.getId().equals(postId)) {
                            postService.deletePost(postId);
                            break;
                        }
                    }
                } catch (ProjectException e) {

                }
            } else if (TypeUser.ADMIN.equals(role)) {
                try {
                    postService.deletePost(postId);

                } catch (ProjectException e) {

                }
            }
        } catch (NumberFormatException e) {
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
            LOGGER.error("invalid id");
        }

        return router;
    }

}
