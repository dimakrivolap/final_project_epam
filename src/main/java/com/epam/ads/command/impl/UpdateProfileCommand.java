package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.UserService;
import com.epam.ads.validation.Validator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class UpdateProfileCommand implements Command {

    private UserService userService;

    public UpdateProfileCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        HttpSession session = request.getSession();

        User user = (User) session.getAttribute(ATTRIBUTE_USER);
        String login = user.getLogin();
        String oldPassword = request.getParameter(PARAM_NAME_OLD_PASSWORD);
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        String firstName = request.getParameter(PARAM_NAME_FIRST_NAME).replaceAll("</?script>", "");
        String lastName = request.getParameter(PARAM_NAME_LAST_NAME).replaceAll("</?script>", "");
        String email = request.getParameter(PARAM_NAME_EMAIL).replaceAll("</?script>", "");
        String phoneNumber = request.getParameter(PARAM_NAME_PHONE_NUMBER).replaceAll("</?script>", "");

        Locale locale = ((Locale) session.getAttribute(ATTRIBUTE_LOCALE));
        String language = locale.getLanguage().toUpperCase();
        try {
            user = userService.checkUser(login, oldPassword);
        } catch (ProjectException e) {
            user = null;
        }
        if (user != null) {
            user.setPassword(password);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPhoneNumber(phoneNumber);
            if (Validator.chechUserProfile(user))
                try {

                    userService.updateProfile(user);
                    session.setAttribute(ATTRIBUTE_USER, user);
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));

                } catch (ProjectException e) {
                    request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                            MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
                    router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_PROFILE));
                }
        } else {
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(language).getMessage(MESSAGE_LOGIN_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_PROFILE));
        }
        return router;
    }

}
