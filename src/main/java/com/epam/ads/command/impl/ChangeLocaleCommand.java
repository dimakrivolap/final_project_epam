package com.epam.ads.command.impl;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.controller.Router;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.resource.ConfigurationManager;
import com.epam.ads.resource.MessageManager;
import com.epam.ads.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

import static com.epam.ads.command.CommandConstant.*;
/**
 *
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 * @see CommandType
 */
public class ChangeLocaleCommand implements Command {

    private UserService userService;

    public ChangeLocaleCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Router execute(HttpServletRequest request) {
        Router router = new Router();
        String language = request.getParameter(PARAM_NAME_LOCALE_VALUE);
        HttpSession session = request.getSession();
        User user = (User) (session.getAttribute(ATTRIBUTE_USER));
        Locale oldLocale = ((Locale) session.getAttribute(ATTRIBUTE_LOCALE));
        String oldLanguage = oldLocale.getLanguage().toUpperCase();
        router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        try {
            Locale locale = MessageManager.valueOf(language).getLocale();
            session.setAttribute(ATTRIBUTE_LOCALE, locale);
            if (user != null) {
                userService.updateLocale(user, language);
                router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_CLIENT_MAIN));
                router.setRoute(Router.RouteType.REDIRECT);
            }
        } catch (IllegalArgumentException e) {
            session.setAttribute(ATTRIBUTE_LOCALE, oldLocale);
            request.setAttribute(ATTRIBUTE_ERROR_LOGIN_OR_PASSWORD,
                    MessageManager.valueOf(oldLanguage).getMessage(MESSAGE_LOGIN_ERROR));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_LOGIN));
        } catch (ProjectException e) {
            request.setAttribute(ATRIBUTE_WRONG_ACTION,
                    MessageManager.valueOf(language).getMessage(MESSAGE_WRONG_ACTION));
            router.setPagePath(ConfigurationManager.getProperty(PATH_PAGE_MAIN));
        }
        return router;
    }
}
