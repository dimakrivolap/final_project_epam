package com.epam.ads.command;


import com.epam.ads.command.impl.*;
import com.epam.ads.service.*;

import java.util.Arrays;
import java.util.Optional;
/**
 * Contains All commands in project
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see Command
 */
public enum CommandType {
    LOGIN(new LoginCommand(new UserService(), new MarkService())),
    LOGOUT(new LogoutCommand()),
    REGISTRATION(new RegistrationCommand(new RegistrationService())),
    USER_POSTS(new UserPostsCommand(new PostService())),
    UPDATE_PROFILE(new UpdateProfileCommand(new UserService())),
    CHANGE_LOCALE(new ChangeLocaleCommand(new UserService())),
    CHANGE_MARK(new ChangeMarkCommand(new MarkService())),
    GET_USER_THEMES(new ChangeLocaleCommand(new UserService())),
    GET_ALL_THEMES(new AllThemesCommand(new PostService())),
    GET_POSTS(new PostsCommand(new PostService())),
    GET_POST(new PostCommand(new PostService(), new UserService(), new CommentService())),
    NEW_POST(new NewPostCommand(new PostService())),
    ABOUT(new PageAboutCommand()),
    SEND_MESSAGE_PAGE(new SendMessagePageCommand()),
    SEND_MESSAGE(new SendMessageCommand(new MessageService(), new UserService())),
    GET_USER(new UserCommand(new UserService(), new MarkService())),
    GET_SEARCH_PAGE(new SearchPageCommand(new PostService())),
    CREATE_POST(new CreatePostCommand(new PostService())),
    CREATE_COMMENT(new CreateCommentCommand(new CommentService(), new PostService(), new UserService())),
    DELETE_POST(new DeletePostCommand(new PostService())),
    DELETE_COMMENT(new DeleteCommentCommand(new CommentService(), new PostService(), new UserService())),
    MODIFY_POST(new ModifyPostCommand(new PostService())),
    UPDATE_POST(new UpdatePostCommand(new PostService())),
    GET_MESSAGES(new MessagesCommand(new MessageService(), new UserService())),
    PASSWORD_RETRIEVAL(new PasswordRetrievalCommand(new UserService())),
    FIND_POST(new FindPostCommand(new PostService())),
    CHANGE_AVAILABLE(new ChangeAvailableCommand(new UserService()));
    private Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public static Optional<Command> getCommand(String commandName) {
        return Arrays.stream(CommandType.values())
                .filter(o -> o.name().equalsIgnoreCase(commandName))
                .map(CommandType::getCommand)
                .findAny();
    }

    public Command getCommand() {
        return command;
    }
}
