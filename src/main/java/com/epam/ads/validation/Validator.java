package com.epam.ads.validation;

import com.epam.ads.entity.User;
import com.epam.ads.resource.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;
import java.util.regex.Pattern;


/**
 * Validates data such as e-mail, name and password, by checking it against
 * predefined patterns. Utility class, therefore final as it's not designed for
 * instantiation and extension.
 *
 * @author Dmitry Krivolap
 * @since Sep 31, 2018
 */
public final class Validator {

    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private final static String PASSWORD_REGEX = ResourceBundle.getBundle("regexp").getString("PASSWORD_REGEX");
    private final static String FULL_NAME_REGEX = ResourceBundle.getBundle("regexp").getString("FULL_NAME_REGEX");
    private final static String EMAIL_REGEX = ResourceBundle.getBundle("regexp").getString("EMAIL_REGEX");
    private final static String LOGIN_REGEX = ResourceBundle.getBundle("regexp").getString("LOGIN_REGEX");
    private final static String PHONE_NUMBER_REGEX = ResourceBundle.getBundle("regexp").getString("PHONE_NUMBER_REGEX");

    /**
     * Default private constructor as this class entirely utility.
     */
    private Validator() {
    }

    /**
     * Checks the name against user's full name pattern.
     *
     * @param fullName user's full name.
     * @return True in case of success and false otherwise.
     */
    public static boolean checkName(String fullName) {
        boolean isValid = fullName != null && Pattern.compile(FULL_NAME_REGEX).matcher(fullName).matches();
        LOGGER.info("User name is valid: " + isValid);
        return isValid;
    }

    /**
     * Checks the e-mail against user's e-mail pattern.
     *
     * @param email user's e-mail.
     * @return True in case of success and false otherwise.
     */
    public static boolean checkEmail(String email) {
        boolean isValid = email != null && Pattern.compile(EMAIL_REGEX).matcher(email).matches();
        LOGGER.info("User e-mail is valid: " + isValid);
        return isValid;
    }

    /**
     * Checks the login against user's login pattern.
     *
     * @param login user's login.
     * @return True in case of success and false otherwise.
     */
    public static boolean checkLogin(String login) {
        boolean isValid = login != null && Pattern.compile(LOGIN_REGEX).matcher(login).matches();
        LOGGER.info("User login is valid: " + isValid);
        return isValid;
    }

    /**
     * Checks the password against user's password pattern.
     *
     * @param password user's password.
     * @return True in case of success and false otherwise.
     */
    public static boolean checkPassword(String password) {
        boolean isValid = password != null && Pattern.compile(PASSWORD_REGEX).matcher(password).matches();
        LOGGER.info("User password is valid: " + isValid);
        return isValid;
    }

    /**
     * Checks the password, e-mail and name against user's password, e-mail and name
     * pattern respectively.
     *
     * @param user user which is meant to be checked.
     * @return True in case of success and false otherwise.
     */
    public static boolean checkUserRegistration(User user) {
        return checkLogin(user.getLogin()) &&
                checkEmail(user.getEmail()) &&
                checkName(user.getFullName()) &&
                checkPassword(user.getPassword()) &&
                checkPhoneNumber(user.getPhoneNumber());
    }


    /**
     * Checks the phone number against user's phone number pattern.
     *
     * @param phoneNumber user's phone number.
     * @return True in case of success and false otherwise.
     */
    private static boolean checkPhoneNumber(String phoneNumber) {
        boolean isValid = phoneNumber != null && Pattern.compile(PHONE_NUMBER_REGEX).matcher(phoneNumber).matches();
        LOGGER.info("User phone number is valid: " + isValid);
        return isValid;
    }

    /**
     * Checks the password and e-mail and login against user's password and e-mail and login pattern
     * respectively.
     *
     * @param user user which is meant to be checked.
     * @return True in case of success and false otherwise.
     */
    public static boolean checkUserLogin(User user) {
        return checkPassword(user.getPassword()) &&
                checkEmail(user.getEmail()) &&
                checkLogin(user.getLogin());
    }

    public static boolean chechUserProfile(User user) {
        return checkPassword(user.getPassword()) &&
                checkEmail(user.getEmail()) &&
                checkName(user.getFullName()) &&
                checkPhoneNumber(user.getPhoneNumber());
    }

    public static boolean checkLocale(String locale) {
        String language = locale.substring(0, 2).toUpperCase();
        for (MessageManager manager : MessageManager.values()) {
            if (manager.name().equals(language)) {
                return true;
            }
        }
        return false;
    }
}
