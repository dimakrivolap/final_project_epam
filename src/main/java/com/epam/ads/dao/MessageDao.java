package com.epam.ads.dao;

import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.entity.Message;
import com.epam.ads.exception.ProjectException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * MessageDao.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see AbstractDao
 */
public class MessageDao extends AbstractDao<Integer, Message> {

    private static final String SQL_SELECT_RESEIVER_MESSAGES_BY_USER_ID_AND_START = "SELECT id,id_user_sender,id_user_receiver,text,date_created FROM message WHERE id_user_receiver=? ORDER BY date_created DESC LIMIT 5 OFFSET ?";
    private static final String SQL_SELECT_SENT_MESSAGES_BY_USER_ID_AND_START = "SELECT id,id_user_sender,id_user_receiver,text,date_created FROM message WHERE id_user_sender=? ORDER BY date_created DESC LIMIT 5 OFFSET ?";
    private static final String SQL_COUNT_RECEIVED_MESSAGES_BY_USER_ID = "SELECT COUNT( * ) as \"count\" FROM message WHERE id_user_receiver=?";
    private static final String SQL_COUNT_SENT_MESSAGES_BY_USER_ID = "SELECT COUNT( * ) as \"count\" FROM message WHERE id_user_sender=?";
    private static final String SQL_CREATE_MESSAGE = "INSERT INTO message(id_user_sender,id_user_receiver,text) VALUE (?,?,?)";
    // column labels
    private static final String ID = "id";
    private static final String ID_USER_SENDER = "id_user_sender";
    private static final String ID_USER_RECEIVER = "id_user_receiver";
    private static final String TEXT = "text";
    private static final String DATE_CREATED = "date_created";
    private static final String COUNT = "count";

    @Override
    public List<Message> findAll() throws ProjectException {
        return null;
    }

    @Override
    public Optional<Message> findById(Integer id) throws ProjectException {
        return Optional.empty();
    }

    @Override
    public boolean delete(Integer id) throws ProjectException {
        return false;
    }

    @Override
    public boolean delete(Message entity) throws ProjectException {
        return false;
    }

    @Override
    public boolean create(Message entity) throws ProjectException {
        return false;
    }

    @Override
    public boolean update(Message entity) throws ProjectException {
        return false;
    }


    public List<Message> findReseivedMessagesByIdUser(int idUser, int start) throws ProjectException {
        List<Message> messages = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_RESEIVER_MESSAGES_BY_USER_ID_AND_START);
            st.setInt(1, idUser);
            st.setInt(2, start);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Message message = setMessage(resultSet);
                messages.add(message);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return messages;
    }

    public List<Message> findSentMessagesByIdUser(int idUser, int start) throws ProjectException {
        List<Message> messages = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_SENT_MESSAGES_BY_USER_ID_AND_START);
            st.setInt(1, idUser);
            st.setInt(2, start);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Message message = setMessage(resultSet);
                messages.add(message);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return messages;
    }

    private Message setMessage(ResultSet resultSet) throws SQLException {
        Message message = null;
        if (resultSet != null) {
            message = new Message();
            message.setId(resultSet.getInt(ID));
            message.setIdUserSender(resultSet.getInt(ID_USER_SENDER));
            message.setIdUserReceiver(resultSet.getInt(ID_USER_RECEIVER));
            message.setText(resultSet.getString(TEXT));
            message.setDateCreated(resultSet.getTimestamp(DATE_CREATED).toLocalDateTime());
        }
        return message;
    }

    public int findCountReceivedMessagesByIdUser(Integer idUser) throws ProjectException {
        int count = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_COUNT_RECEIVED_MESSAGES_BY_USER_ID);
            st.setInt(1, idUser);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            count = resultSet.getInt(COUNT);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return count;
    }

    public int findCountSentMessagesByIdUser(Integer idUser) throws ProjectException {
        int count = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_COUNT_SENT_MESSAGES_BY_USER_ID);
            st.setInt(1, idUser);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            count = resultSet.getInt(COUNT);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return count;
    }

    public void createMessage(int idSender, int idReceiver, String message) throws ProjectException {
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_CREATE_MESSAGE);
            st.setInt(1, idSender);
            st.setInt(2, idReceiver);
            st.setString(3, message);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
    }
}
