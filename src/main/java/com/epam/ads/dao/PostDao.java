package com.epam.ads.dao;

import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.entity.TypePost;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * PostDao.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see AbstractDao
 */
public class PostDao extends AbstractDao<Integer, Post> {
    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger(PostDao.class);

    private static final String SQL_SELECT_ALL_POSTS = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post";
    private static final String SQL_SELECT_ALL_POSTS_BY_USER_ID = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE user_id=?";
    private static final String SQL_SELECT_ALL_POSTS_BY_LOGIN = "SELECT P.id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post as P join user ON user.id=P.id_user WHERE user.login=?";
    private static final String SQL_SELECT_ALL_POSTS_BY_THEME_ID = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE id_theme_post=?";
    private static final String SQL_SELECT_POSTS_BY_THEME_ID_AND_START = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE id_theme_post=? LIMIT 5 OFFSET ?";
    private static final String SQL_SELECT_POSTS_BY_TYPE_POST_AND_PRICE = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE type_post=? AND price between ? and ?";
    private static final String SQL_SELECT_THEME_BY_ID = "SELECT id,name FROM theme_post WHERE id=?";
    private static final String SQL_COUNT_POSTS_BY_THEME_ID = "SELECT COUNT( * ) as \"count\" FROM post WHERE id_theme_post=?";
    private static final String SQL_SELECT_ALL_THEMES_BY_LOCALE = "SELECT id,name FROM theme_post WHERE language=?";
    private static final String SQL_SELECT_ALL_POSTS_BY_TIME_CREATED = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE date_created BETWEEN ? and ?";
    private static final String SQL_SELECT_ALL_POSTS_BY_TIME_UPDATED = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE last_updated BETWEEN ? and ?";
    private static final String SQL_SELECT_POST_BY_ID = "SELECT id,id_theme_post,id_user,type_post,mini_description,price,context,date_created,last_updated FROM post WHERE id=?";
    private static final String SQL_CREATE_POST = "INSERT INTO post(id_theme_post,id_user,type_post,mini_description,price,context) VALUE (?,?,?,?,?,?)";
    private static final String SQL_UPDATE_POST_BY_ID = "UPDATE post SET id_theme_post = ?, type_post = ? ,mini_description =?,price=?,context=?,last_updated =? WHERE id=?";
    private static final String SQL_DELETE_POST_BY_ID = "DELETE FROM post WHERE id=?";


    // column labels
    private static final String ID = "id";
    private static final String ID_THEME_POST = "id_theme_post";
    private static final String THEME_NAME = "name";
    private static final String ID_USER = "id_user";
    private static final String TYPE_POST = "type_post";
    private static final String MINI_DESCRIPTION = "mini_description";
    private static final String PRICE = "price";
    private static final String CONTEXT = "context";
    private static final String DATE_CREATED = "date_created";
    private static final String LAST_UPDATED = "last_updated";
    private static final String COUNT = "count";

    @Override
    public List<Post> findAll() throws ProjectException {
        List<Post> posts = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_POSTS);
            while (resultSet.next()) {
                Post post = setPost(resultSet);
                posts.add(post);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return posts;
    }

    public List<Post> findByThemePostId(Integer idTheme) throws ProjectException {
        List<Post> posts = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_POSTS_BY_THEME_ID);
            st.setLong(1, idTheme);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Post post = setPost(resultSet);
                posts.add(post);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return posts;
    }

    public List<Post> findByLogin(String login) throws ProjectException {
        List<Post> posts = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_POSTS_BY_LOGIN);
            st.setString(1, login);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Post post = setPost(resultSet);
                posts.add(post);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return posts;
    }

    public List<Post> findByUserId(Integer idUser) throws ProjectException {
        List<Post> posts = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_POSTS_BY_USER_ID);
            st.setLong(1, idUser);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Post post = setPost(resultSet);
                posts.add(post);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return posts;
    }

    @Override
    public Optional<Post> findById(Integer id) throws ProjectException {
        Post post = null;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_POST_BY_ID);
            st.setInt(1, id);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            post = setPost(resultSet);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return Optional.ofNullable(post);
    }


    @Override
    public boolean delete(Integer id) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_DELETE_POST_BY_ID);
            st.setLong(1, id);
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("Delete post error", e);
        } finally {
            close(st);
            close(cn);
        }
        return result != 0;
    }

    @Override
    public boolean delete(Post post) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_DELETE_POST_BY_ID);
            st.setLong(1, post.getId());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("Delete post error", e);
        } finally {
            close(st);
            close(cn);
        }
        return result != 0;
    }

    @Override
    public boolean create(Post post) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_CREATE_POST);
            st.setLong(1, post.getIdThemePost());
            st.setLong(2, post.getIdUser());
            st.setString(3, post.getTypePost().name());
            st.setString(4, post.getMiniDescription());
            st.setBigDecimal(5, post.getPrice());
            st.setString(6, post.getContext());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }

        return result != 0;
    }

    @Override
    public boolean update(Post post) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_UPDATE_POST_BY_ID);
            st.setLong(1, post.getIdThemePost());
            st.setString(2, post.getTypePost().name());
            st.setString(3, post.getMiniDescription());
            st.setBigDecimal(4, post.getPrice());
            st.setString(5, post.getContext());
            st.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
            st.setInt(7, post.getId());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }

        return result != 0;
    }

    public List<ThemePost> findAllThemes(String locale) throws ProjectException {
        List<ThemePost> themes = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_ALL_THEMES_BY_LOCALE);
            st.setString(1, locale);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                ThemePost themePost = setThemePost(resultSet);
                themes.add(themePost);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return themes;
    }

    private Post setPost(ResultSet resultSet) throws SQLException {
        Post post = null;
        if (resultSet != null) {
            post = new Post();
            post.setId(resultSet.getInt(ID));
            post.setIdThemePost(resultSet.getInt(ID_THEME_POST));
            post.setIdUser(resultSet.getInt(ID_USER));
            post.setTypePost(TypePost.valueOf(resultSet.getString(TYPE_POST).toUpperCase()));
            post.setMiniDescription(resultSet.getString(MINI_DESCRIPTION));
            post.setPrice(resultSet.getBigDecimal(PRICE));
            post.setContext(resultSet.getString(CONTEXT));
            post.setLastUpdate(resultSet.getTimestamp(LAST_UPDATED).toLocalDateTime());
            post.setDateCreated(resultSet.getTimestamp(DATE_CREATED).toLocalDateTime());
        }
        return post;
    }

    private ThemePost setThemePost(ResultSet resultSet) throws SQLException {
        ThemePost themePost = null;

        if (resultSet != null) {
            themePost = new ThemePost();
            themePost.setId(resultSet.getInt(ID));
            themePost.setName(resultSet.getString(THEME_NAME));
        }
        return themePost;
    }

    public List<Post> findByThemePostIdAndStart(int themeId, int start) throws ProjectException {
        List<Post> posts = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_POSTS_BY_THEME_ID_AND_START);
            st.setInt(1, themeId);
            st.setInt(2, start);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Post post = setPost(resultSet);
                posts.add(post);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return posts;
    }

    public Optional<ThemePost> findThemeById(int themeId) throws ProjectException {
        ThemePost themePost;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_THEME_BY_ID);
            st.setLong(1, themeId);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            themePost = setThemePost(resultSet);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return Optional.ofNullable(themePost);
    }

    public int findCountPostsByThemeId(int themeId) throws ProjectException {
        int count = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_COUNT_POSTS_BY_THEME_ID);
            st.setInt(1, themeId);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            count = resultSet.getInt(COUNT);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return count;
    }

    public List<Post> findPost(String typePost, BigDecimal priceMin, BigDecimal priceMax) throws ProjectException {
        List<Post> posts = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_POSTS_BY_TYPE_POST_AND_PRICE);
            st.setString(1, typePost);
            st.setBigDecimal(2, priceMin);
            st.setBigDecimal(3, priceMax);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Post post = setPost(resultSet);
                posts.add(post);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return posts;
    }


    //   public List<Message>

}
