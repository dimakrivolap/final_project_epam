package com.epam.ads.dao;

import com.epam.ads.entity.Entity;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;
/**
 * AbstractDao.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 */
public abstract class AbstractDao<K, T extends Entity> {
    private static final Logger LOGGER = LogManager.getLogger(AbstractDao.class);

    public abstract List<T> findAll() throws ProjectException;

    public abstract Optional<T> findById(K id) throws ProjectException;

    public abstract boolean delete(K id) throws ProjectException;

    public abstract boolean delete(T entity) throws ProjectException;

    public abstract boolean create(T entity) throws ProjectException;

    public abstract boolean update(T entity) throws ProjectException;

    public void close(Statement st) throws ProjectException {
        try {
            if (st != null) {
                st.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Error close Statement");
            throw new ProjectException("Error close Statement", e);
        }
    }

    public void close(Connection connection) throws ProjectException {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Error close Connection");
            throw new ProjectException("Error close Connection", e);
        }
    }
}
