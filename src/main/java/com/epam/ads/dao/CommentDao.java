package com.epam.ads.dao;


import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.entity.Comment;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * CommentDao.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see AbstractDao
 */
public class CommentDao extends AbstractDao<Integer, Comment> {

    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private static final String SQL_SELECT_COMMENTS_BY_USER_ID = "SELECT id,user_id,post_id,text,last_updated FROM comment WHERE user_id =?";
    private static final String SQL_SELECT_COMMENTS_BY_POST_ID = "SELECT id,id_user,id_post,text,last_updated FROM comment WHERE id_post =?";
    private static final String SQL_CREATE_COMMENT = "INSERT INTO comment(id_user,id_post,text) VALUE (?,?,?)";
    private static final String SQL_DELETE_COMMENT_BY_ID = "DELETE FROM comment WHERE id=?";


    // column labels
    private static final String ID = "id";
    private static final String ID_USER = "id_user";
    private static final String ID_POST = "id_post";
    private static final String TEXT = "text";
    private static final String LAST_UPDATED = "last_updated";


    @Override
    public List<Comment> findAll() throws ProjectException {
        return null;
    }

    @Override
    public Optional<Comment> findById(Integer id) throws ProjectException {
        return Optional.empty();
    }

    @Override
    public boolean delete(Integer id) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_DELETE_COMMENT_BY_ID);
            st.setLong(1, id);
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("Delete comment error", e);
        } finally {
            close(st);
            close(cn);
        }
        return result != 0;
    }

    @Override
    public boolean delete(Comment entity) throws ProjectException {
        return false;
    }

    @Override
    public boolean create(Comment comment) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_CREATE_COMMENT);
            st.setInt(1, comment.getIdUser());
            st.setLong(2, comment.getIdPost());
            st.setString(3, comment.getText());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }

        return result != 0;
    }

    @Override
    public boolean update(Comment entity) throws ProjectException {
        return false;
    }

    public List<Comment> findCommentsByPostId(int postId) throws ProjectException {
        List<Comment> comments = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_COMMENTS_BY_POST_ID);
            st.setLong(1, postId);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Comment comment = setComment(resultSet);
                comments.add(comment);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return comments;
    }

    private Comment setComment(ResultSet resultSet) throws SQLException {
        Comment comment = null;
        if (resultSet != null) {
            comment = new Comment();
            comment.setId(resultSet.getInt(ID));
            comment.setIdUser(resultSet.getInt(ID_USER));
            comment.setIdPost(resultSet.getInt(ID_POST));
            comment.setText(resultSet.getString(TEXT));
            comment.setLastUpdate(resultSet.getTimestamp(LAST_UPDATED).toLocalDateTime());
        }
        return comment;
    }
}
