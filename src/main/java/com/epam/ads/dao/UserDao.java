package com.epam.ads.dao;


import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * UserDao.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see AbstractDao
 */
public class UserDao extends AbstractDao<Integer, User> {
    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger(UserDao.class);
    private static final String SQL_SELECT_ALL_USERS = "SELECT id,type,login,password,first_name,last_name,email,phone_number,available FROM user";
    private static final String SQL_SELECT_USER_BY_ID = "SELECT id,type,login,password,first_name,last_name,email,phone_number,available FROM user WHERE user.id=?";
    private static final String SQL_CREATE_USER = "INSERT INTO user(login,password,first_name,last_name,email,phone_number) VALUE (?,?,?,?,?,?)";
    private static final String SQL_SELECT_USER_BY_LOGIN = "SELECT id,type,login,password,first_name,last_name,email,phone_number,available FROM user WHERE user.login=?";
    private static final String SQL_SELECT_USER_BY_EMAIL_AND_PASSWORD = "";//??
    private static final String SQL_UPDATE_USER_BY_ID = "UPDATE user SET password = ? ,first_name =?,last_name=?,email =?,phone_number=? WHERE id=?";
    private static final String SQL_UPDATE_LOCALE = "UPDATE user SET language = ? WHERE login=?";
    private static final String SQL_UPDATE_PASSWORD_BY_EMAIL = "UPDATE user SET password = ? WHERE email=?";
    private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM user where id=?";
    private static final String SQL_BLOCK_USER_BY_ID = "UPDATE user SET available = ? WHERE id=?";


    // column labels
    private static final String ID = "id";
    private static final String TYPE = "type";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String EMAIL = "email";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String AVAILABLE = "available";


    @Override
    public List<User> findAll() throws ProjectException {
        List<User> users = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.createStatement();
            ResultSet resultSet =
                    st.executeQuery(SQL_SELECT_ALL_USERS);
            while (resultSet.next()) {
                User user = setUser(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return users;
    }

    @Override
    public Optional<User> findById(Integer id) throws ProjectException {
        User user = null;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_USER_BY_ID);
            st.setLong(1, id);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            user = setUser(resultSet);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return Optional.ofNullable(user);
    }

    public Optional<User> findUserByLogin(String login) throws ProjectException {
        User user = null;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_USER_BY_LOGIN);
            st.setString(1, login);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            user = setUser(resultSet);
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            ConnectionPool.getInstance().releaseConnection(cn);
        }
        return Optional.ofNullable(user);
    }

    @Override
    public boolean create(User user) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_CREATE_USER);

            st.setString(1, user.getLogin());
            st.setString(2, user.getPassword());
            st.setString(3, user.getFirstName());
            st.setString(4, user.getLastName());
            st.setString(5, user.getEmail());
            st.setString(6, user.getPhoneNumber());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }

        return result != 0;
    }

    @Override
    public boolean update(User user) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_UPDATE_USER_BY_ID);
            st.setString(1, user.getPassword());
            st.setString(2, user.getFirstName());
            st.setString(3, user.getLastName());
            st.setString(4, user.getEmail());
            st.setString(5, user.getPhoneNumber());
            st.setLong(6, user.getId());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }

        return result != 0;
    }

    private User setUser(ResultSet resultSet) throws SQLException {
        User user = null;
        if (resultSet != null) {
            user = new User(resultSet.getInt(ID));
            user.setTypeUser(TypeUser.valueOf(resultSet.getString(TYPE).toUpperCase()));
            user.setLogin(resultSet.getString(LOGIN));
            user.setPassword(resultSet.getString(PASSWORD));
            user.setFirstName(resultSet.getString(FIRST_NAME));
            user.setLastName(resultSet.getString(LAST_NAME));
            user.setEmail(resultSet.getString(EMAIL));
            user.setPhoneNumber(resultSet.getString(PHONE_NUMBER));
            user.setAvailable(resultSet.getBoolean(AVAILABLE));
        }
        return user;
    }

    @Override
    public boolean delete(Integer id) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_DELETE_USER_BY_ID);
            st.setLong(1, id);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("Delete user error", e);
        } finally {
            close(st);
            close(cn);
        }
        return result != 0;
    }

    @Override
    public boolean delete(User user) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_DELETE_USER_BY_ID);
            st.setLong(1, user.getId());
            result = st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("Delete user error", e);
        } finally {
            close(st);
            close(cn);
        }
        return result != 0;
    }

    public boolean updateLocale(User user, String locale) throws ProjectException {
        int result = 0;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_UPDATE_LOCALE);
            st.setString(1, locale);
            st.setString(2, user.getLogin());
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("update locale for user error", e);
        } finally {
            close(st);
            close(cn);
        }
        return result != 0;
    }


    public void updatePassword(String email, String password) throws ProjectException {
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_UPDATE_PASSWORD_BY_EMAIL);
            st.setString(1, password);
            st.setString(2, email);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("update password for user error", e);
        } finally {
            close(st);
            close(cn);
        }
    }

    public void updateAvailable(int idUser, boolean available) throws ProjectException {
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_BLOCK_USER_BY_ID);
            st.setBoolean(1, available);
            st.setInt(2, idUser);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("update available for user error", e);
        } finally {
            close(st);
            close(cn);
        }
    }
}
