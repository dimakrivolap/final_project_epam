package com.epam.ads.dao;

import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.entity.Mark;
import com.epam.ads.exception.ProjectException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
/**
 * MarkDao.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see AbstractDao
 */
public class MarkDao extends AbstractDao<Integer, Mark> {
    private static final String SQL_SELECT_MARK_BY_ID_USER_RECEIVER_AND_ID_USER_SENDER = "SELECT id_user_sender,id_user_receiver,mark_value FROM mark WHERE id_user_sender=? AND id_user_receiver=?";
    private static final String SQL_SELECT_MARKS_BY_ID_USER = "SELECT id_user_sender,id_user_receiver,mark_value FROM mark WHERE id_user_receiver=?";
    private static final String SQL_CHANGE_MARK = "INSERT INTO mark (id_user_sender,id_user_receiver,mark_value) VALUES (?,?,?) ON DUPLICATE KEY UPDATE mark_value=VALUES(mark_value)";

    //column mark
    private static final String ID_USER_SENDER = "id_user_sender";
    private static final String ID_USER_RECEIVER = "id_user_receiver";
    private static final String MARK_VALUE = "mark_value";


    public Mark findMarkByIdUserSenderAndIdUserReceiver(int idUserSender, int idUserReceiver) throws ProjectException {
        Mark mark = null;
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_MARK_BY_ID_USER_RECEIVER_AND_ID_USER_SENDER);
            st.setInt(1, idUserSender);
            st.setInt(2, idUserReceiver);
            ResultSet resultSet = st.executeQuery();
            if (resultSet.next()) {
                mark = setMark(resultSet);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return mark;


    }

    private Mark setMark(ResultSet resultSet) throws SQLException {
        Mark mark = null;
        if (resultSet != null) {
            mark = new Mark();
            mark.setIdUserSender(resultSet.getInt(ID_USER_SENDER));
            mark.setIdUserReceiver(resultSet.getInt(ID_USER_RECEIVER));
            mark.setMarkValue(resultSet.getInt(MARK_VALUE));
        }
        return mark;
    }

    @Override
    public List<Mark> findAll() throws ProjectException {
        return null;
    }

    @Override
    public Optional<Mark> findById(Integer id) throws ProjectException {
        return Optional.empty();
    }

    @Override
    public boolean delete(Integer id) throws ProjectException {
        return false;
    }

    @Override
    public boolean delete(Mark entity) throws ProjectException {
        return false;
    }

    @Override
    public boolean create(Mark entity) throws ProjectException {
        return false;
    }

    @Override
    public boolean update(Mark entity) throws ProjectException {
        return false;
    }

    public List<Mark> findMarksByIdUser(int idUser) throws ProjectException {
        List<Mark> marks = new ArrayList<>();
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_SELECT_MARKS_BY_ID_USER);
            st.setInt(1, idUser);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                Mark mark = setMark(resultSet);
                marks.add(mark);
            }
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
        return marks;
    }

    public void changeMark(int idUserSender, int idUserReceiver, int valueMark) throws ProjectException {
        Connection cn = null;
        PreparedStatement st = null;
        try {
            cn = ConnectionPool.getInstance().takeConnection();
            st = cn.prepareStatement(SQL_CHANGE_MARK);
            st.setInt(1, idUserSender);
            st.setInt(2, idUserReceiver);
            st.setInt(3, valueMark);
            st.executeUpdate();
        } catch (SQLException e) {
            throw new ProjectException("SQL exception (request or table failed): ", e);
        } finally {
            close(st);
            close(cn);
        }
    }
}
