package com.epam.ads.tag;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * Custom tags and functions for Date.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public final class Dates {
    private Dates() {
    }

    public static String formatLocalDateTime(LocalDateTime localDateTime, String pattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
    }
}
