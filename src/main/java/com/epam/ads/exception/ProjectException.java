package com.epam.ads.exception;

public class ProjectException extends Exception {
    private static final long serialVersionUID = 1L;

    public ProjectException(String message, Exception e) {
        super(message, e);
    }

    public ProjectException() {

    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public ProjectException(String message) {
        super(message);
    }
}
