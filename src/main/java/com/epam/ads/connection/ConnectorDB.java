package com.epam.ads.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
/**
 * Main Connector to Data Base.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 */
class ConnectorDB {
    private static final Logger LOGGER = LogManager.getLogger(ConnectorDB.class);

    static ProxyConnection getConnection() {
        try {
            ResourceBundle resource = ResourceBundle.getBundle("database");
            String url = resource.getString("db.url");
            String user = resource.getString("db.user");
            String pass = resource.getString("db.password");
            ProxyConnection proxyConnection = new ProxyConnection(DriverManager.getConnection(url, user, pass));
            return proxyConnection;
        } catch (SQLException e) {
            LOGGER.fatal("create connection Error");
            throw new RuntimeException("create connection Error");
        }

    }
}
