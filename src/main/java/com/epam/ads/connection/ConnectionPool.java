package com.epam.ads.connection;

import com.epam.ads.command.Command;
import com.epam.ads.command.CommandType;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
/**
 * Provides access to connections in the pool. This class was created with use
 * of singleton pattern.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see ProxyConnection
 * @see ConnectorDB
 */
public class ConnectionPool {
    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private static AtomicBoolean instanceCreated = new AtomicBoolean();
    private static ConnectionPool instance;
    private static ReentrantLock lock = new ReentrantLock();
    private final int DEFAULT_POOL_SIZE = 10;
    private LinkedBlockingDeque<ProxyConnection> freeConnectionDueue;
    private LinkedBlockingDeque<ProxyConnection> busyConnectionDueue;

    private ConnectionPool() throws SQLException {
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());

        try {
            Driver driver = new com.mysql.jdbc.Driver();
            DriverManager.registerDriver(driver);
            LOGGER.debug("driver " + driver + " registered...");
        } catch (SQLException e) {
            LOGGER.fatal("Driver registration error", e);
            throw new RuntimeException();
        }
        freeConnectionDueue = new LinkedBlockingDeque<>(DEFAULT_POOL_SIZE);
        busyConnectionDueue = new LinkedBlockingDeque<>(DEFAULT_POOL_SIZE);
        initPool();
    }

    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    try {
                        instance = new ConnectionPool();
                        instanceCreated.set(true);
                    } catch (SQLException e) {
                        LOGGER.fatal("create ConnectionPool Error");
                        throw new RuntimeException("create ConnectionPool Error");
                    }
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public void initPool() {
        ProxyConnection connection;
        for (int i = 0; i < DEFAULT_POOL_SIZE; i++) {
            connection = ConnectorDB.getConnection();
            freeConnectionDueue.offer(connection);
        }
    }

    public Connection takeConnection() {
        ProxyConnection connection = null;
        try {
            connection = freeConnectionDueue.take();
            busyConnectionDueue.offer(connection);
        } catch (InterruptedException e) {
            LOGGER.error("The thread has been interrupted during fetching a connection" + e);
            Thread.currentThread().interrupt();
        }
        return connection;
    }

    public boolean releaseConnection(Connection connection) throws ProjectException {
        if (connection == null) {
            LOGGER.warn("Someone trying to return null connection.");
            return false;
        }
        try {
            if (!connection.getAutoCommit()) {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProjectException("set Auto commit error", e);
        }
        busyConnectionDueue.remove(connection);
        freeConnectionDueue.offer((ProxyConnection) connection);
        return true;

    }

    public void dispose() {
        closeConnectionsDueue(freeConnectionDueue);
    }

    private void closeConnectionsDueue(LinkedBlockingDeque<ProxyConnection> queue) {
        try {
            ProxyConnection connection;
            for (int i = 0; i < DEFAULT_POOL_SIZE; i++) {
                connection = queue.take();
                connection.closeConnection();
            }
        } catch (InterruptedException e) {
            LOGGER.fatal("Close connection error" + e);
            Thread.currentThread().interrupt();
        } catch (SQLException e) {
            LOGGER.error("SQL error" + e);
        }
    }

    private void deregisterDrivers() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            try {
                Driver driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
                LOGGER.debug("driver deregister" + driver);
            } catch (SQLException e) {
                LOGGER.error("SQL exception", e);
            }
        }
    }


}
