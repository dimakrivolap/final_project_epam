package com.epam.ads.resource;

import java.util.ResourceBundle;
/**
 *
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config");

    private ConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
