package com.epam.ads.resource;

import java.util.Locale;
import java.util.ResourceBundle;
/**
 * Locales and bundle messages
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public enum MessageManager {
    EN(ResourceBundle.getBundle("messages", new Locale("en", "US")), new Locale("en", "US")),
    RU(ResourceBundle.getBundle("messages", new Locale("ru", "RU")), new Locale("ru", "RU")),
    DE(ResourceBundle.getBundle("messages", new Locale("de", "DE")), new Locale("de", "DE"));

    private ResourceBundle bundle;
    private MessageManager manager;
    private Locale locale;

    MessageManager(ResourceBundle bundle, Locale locale) {
        this.bundle = bundle;
        this.locale = locale;
    }

    public String getMessage(String key) {
        return bundle.getString(key);
    }

    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {
        return locale.getLanguage();
    }

    public static boolean contains(String test) {

        for (MessageManager manager : MessageManager.values()) {
            if (manager.locale.getLanguage().equals(test)) {
                return true;
            }
        }
        return false;
    }
}
