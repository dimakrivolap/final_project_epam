package com.epam.ads.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;


@WebFilter(initParams = {@WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param")},
        urlPatterns = {"/*"})

/**
 * This filter sets encoding parameters for this application.
 * @author Dmitry Krivolap
 * @since Sep 31, 2018
 */
public class EncodingFilter implements Filter {

    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Encoding parameter.
     */
    private String encoding;


    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        LOGGER.info("Encoding filter has been initialized");
        encoding = fConfig.getInitParameter("encoding");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String codeRequest = request.getCharacterEncoding();
        if (encoding != null && !encoding.equalsIgnoreCase(codeRequest)) {
            request.setCharacterEncoding(encoding);
            response.setCharacterEncoding(encoding);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOGGER.info("Encoding filter has been destroyed");
        encoding = null;
    }

}