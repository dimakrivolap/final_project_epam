package com.epam.ads.filter;


/**
 * FilterConstant.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 */
public final class FilterConstant {

    /**
     * {@link String} constant for utility use inside this class.
     */
    static final String USER = "user";

    static final String CACHE_CONTROL = "Cache-Control";
    static final String NO_CACHE = "no-cache";
    static final String NO_STORE = "no-store";
    static final String EXPIRES = "Expires";
    static final String PRAGMA = "Pragma";

    static final String ENCODING_PARAM = "encoding";


    /**
     * Constant string for use inside the class.
     */
    static final String LOCALE = "locale";

    /**
     * Default locale for the application.
     */
    static final String DEFAULT_LOCALE = "EN";


    private FilterConstant() {

    }
}