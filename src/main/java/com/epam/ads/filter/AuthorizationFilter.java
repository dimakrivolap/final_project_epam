package com.epam.ads.filter;


import com.epam.ads.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ResourceBundle;

import static com.epam.ads.filter.FilterConstant.USER;


@WebFilter(urlPatterns = {"/jsp/profile.jsp"})
/**
 * Checks if the user has been authorized in this application.
 * @author Dmirtry Krivolap
 * @since Sep 31, 2018
 */

public class AuthorizationFilter implements Filter {

    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    private static final ResourceBundle resource = ResourceBundle.getBundle("config");

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        User user = (User) httpServletRequest.getSession().getAttribute(USER);
        LOGGER.info("Current user: " + user);
        if (user != null) {
            chain.doFilter(request, response);
        } else {
            LOGGER.info("Unauthorizide user is redirected to the index.jsp");
            ((HttpServletResponse) response).sendRedirect(httpServletRequest.getContextPath() + resource.getString("path.page.main"));
        }
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        LOGGER.info("Authorization filter has been initialized");
    }

    @Override
    public void destroy() {
        LOGGER.info("Authorization filter has been destroyed");
    }
}
