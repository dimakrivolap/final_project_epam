package com.epam.ads.filter;

import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import com.epam.ads.service.MarkService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DecimalFormat;
@WebFilter(filterName = "RatingFilter", urlPatterns = {"/main","/controller/main"})
/**
 * RatingFilter.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 * @see User
 */
public class RatingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpSession httpSession = httpServletRequest.getSession();
        DecimalFormat formatter = new DecimalFormat("#0.00");
        try {
            User user = (User)httpSession.getAttribute("user");
            if(user!=null) {
                double rating = new MarkService().getRating(user.getId());
                httpSession.setAttribute("rating", formatter.format(rating));
            }
        }
        catch (ProjectException e){
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
