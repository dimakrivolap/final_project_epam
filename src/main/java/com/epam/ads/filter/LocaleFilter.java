package com.epam.ads.filter;

import java.io.IOException;
import java.util.Locale;
import java.util.stream.Stream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.ads.resource.MessageManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.epam.ads.filter.FilterConstant.*;


@WebFilter(urlPatterns = { "/*" })
/**
 * LocaleFilter.
 * @author Dmitry Krivolap
 * @since Oct 15, 2018
 */
public class LocaleFilter implements Filter {

    /**
     * Instance of {@code org.apache.logging.log4j.Logger} is used for logging.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession httpSession = httpServletRequest.getSession();
        Cookie[] cookies = httpServletRequest.getCookies();
        Locale locale = (Locale) httpSession.getAttribute(LOCALE);
        if (locale == null || !MessageManager.contains(locale.getLanguage())) {
            String value;
            if (cookies!=null) {
                value = Stream.of(cookies)
                        .filter(s -> s.getName().equals(LOCALE))
                        .findFirst().map(Cookie::getValue)
                        .orElse(DEFAULT_LOCALE);
            } else {
                value = DEFAULT_LOCALE;
            }
            locale = MessageManager.valueOf(value).getLocale();
            httpSession.setAttribute(LOCALE, locale);
            LOGGER.info("Locale has been changed to " + value);
        }
        chain.doFilter(request, response);

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.info("Locale filter has been initialized");
    }

    @Override
    public void destroy() {
        LOGGER.info("Locale filter has been destroyed");
    }

}
