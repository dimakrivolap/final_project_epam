package com.epam.ads.listener;

import com.epam.ads.entity.TypeUser;
import com.epam.ads.resource.MessageManager;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;


/**
 * Class SessionRoleListener used to set newly created sessions attribute 'role'
 * to User.Role.GUEST and attribute 'locale' to 'en_US'
 *
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 * @see javax.servlet.http.HttpSessionListener
 */
@WebListener
public class SessionRoleListener implements HttpSessionListener {

    /**
     * Sets newly created sessions attribute 'role' to User.Role.GUEST and
     * attribute 'locale' to 'en_US'
     *
     * @param httpSessionEvent {@link HttpSessionEvent} object
     */
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        HttpSession session = httpSessionEvent.getSession();
        TypeUser role = (TypeUser) session.getAttribute("role");
        if (role == null) {
            session.setAttribute("role", TypeUser.GUEST);
            session.setAttribute("locale", MessageManager.EN.getLocale());
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {

    }
}
