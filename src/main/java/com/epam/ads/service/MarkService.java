package com.epam.ads.service;

import com.epam.ads.dao.MarkDao;
import com.epam.ads.entity.Mark;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
/**
 * MarkService.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class MarkService {
    private static final Logger LOGGER = LogManager.getLogger();

    public double findMark(int idUserSender, int idUserReceiver) throws ProjectException {
        Mark mark;
        try {
            MarkDao markDao = new MarkDao();
            mark = markDao.findMarkByIdUserSenderAndIdUserReceiver(idUserSender, idUserReceiver);
            if (mark != null) {
                return mark.getMarkValue();
            }
        } catch (ProjectException e) {
            LOGGER.error("Service mark error" + e);
            throw new ProjectException("Service error", e);
        }
        return -1;
    }

    public double getRating(int idUser) throws ProjectException {
        double rating = 0;
        List<Mark> marks;
        try {
            MarkDao markDao = new MarkDao();
            marks = markDao.findMarksByIdUser(idUser);
            double sum = 0;
            for (Mark mark : marks) {
                sum += mark.getMarkValue();
            }
            if (marks.size() != 0) {
                rating = sum / marks.size();
            } else {
                rating = 5;
            }
        } catch (ProjectException e) {
            LOGGER.error("Service mark error" + e);
            throw new ProjectException("Service error", e);
        }
        return rating;
    }

    public void changeMark(int idUserSender, int idUserReceiver, int valueMark) throws ProjectException {
        try {
            MarkDao markDao = new MarkDao();
            markDao.changeMark(idUserSender, idUserReceiver, valueMark);
        } catch (ProjectException e) {
            LOGGER.error("Service mark error" + e);
            throw new ProjectException("Service error", e);
        }
    }
}
