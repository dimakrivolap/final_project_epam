package com.epam.ads.service;

import com.epam.ads.dao.CommentDao;
import com.epam.ads.entity.Comment;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
/**
 * CommentService.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class CommentService {
    private static final Logger LOGGER = LogManager.getLogger();

    public List<Comment> getCommentsByPostId(int postId) throws ProjectException {
        CommentDao commentDao = new CommentDao();
        List<Comment> comments;
        try {
            comments = commentDao.findCommentsByPostId(postId);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return comments;
    }

    public void createComment(Comment comment) throws ProjectException {
        CommentDao commentDao = new CommentDao();
        try {
            commentDao.create(comment);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
    }

    public void deleteComment(Integer idComment) throws ProjectException {
        CommentDao commentDao = new CommentDao();
        try {
            commentDao.delete(idComment);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
    }
}
