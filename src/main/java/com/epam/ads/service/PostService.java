package com.epam.ads.service;

import com.epam.ads.dao.PostDao;
import com.epam.ads.entity.Post;
import com.epam.ads.entity.ThemePost;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
/**
 * PostService.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class PostService {
    private static final Logger LOGGER = LogManager.getLogger();

    public List<Post> getPosts(String login) throws ProjectException {
        PostDao postDao = new PostDao();
        List<Post> posts;
        try {
            posts = postDao.findByLogin(login);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return posts;
    }

    public List<ThemePost> getAllThemes(Locale locale) throws ProjectException {
        PostDao postDao = new PostDao();
        List<ThemePost> themes;
        try {
            themes = postDao.findAllThemes(locale.getLanguage());
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return themes;
    }

    public List<Post> getPosts(int themeId, int start) throws ProjectException {
        PostDao postDao = new PostDao();
        List<Post> posts;
        try {
            posts = postDao.findByThemePostIdAndStart(themeId, start);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return posts;
    }

    public int getCountPostsByThemeId(int themeId) throws ProjectException {
        int count = 0;
        PostDao postDao = new PostDao();
        try {
            count = postDao.findCountPostsByThemeId(themeId);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return count;

    }

    public ThemePost getThemeById(int themeId) throws ProjectException {
        PostDao postDao = new PostDao();
        ThemePost theme;
        try {
            theme = postDao.findThemeById(themeId).get();
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return theme;
    }

    public Post getPost(int postId) throws ProjectException {
        PostDao postDao = new PostDao();
        Post post;
        try {
            post = postDao.findById(postId).get();
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return post;
    }

    public void createPost(Post post) throws ProjectException {
        PostDao postDao = new PostDao();
        try {
            postDao.create(post);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
    }

    public void deletePost(Integer postId) throws ProjectException {
        PostDao postDao = new PostDao();
        try {
            postDao.delete(postId);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
    }

    public void updatePost(Post post) throws ProjectException {
        PostDao postDao = new PostDao();
        try {
            postDao.update(post);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
    }

    public List<Post> getPosts(String searchPhrase, String typePost, BigDecimal priceMin, BigDecimal priceMax, int start) throws ProjectException {
        PostDao postDao = new PostDao();
        List<Post> posts;
        List<Post> resultPosts = new ArrayList<>();
        try {
            posts = postDao.findPost(typePost, priceMin, priceMax);
            for (Post post : posts) {
                if ((post.getMiniDescription().contains(searchPhrase)) || (post.getContext().contains(searchPhrase))) {
                    resultPosts.add(post);
                }
            }
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return resultPosts;
    }
}
