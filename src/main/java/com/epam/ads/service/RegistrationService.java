package com.epam.ads.service;

import com.epam.ads.dao.UserDao;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * RegistrationService.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class RegistrationService {
    private static final Logger LOGGER = LogManager.getLogger(RegistrationService.class);

    public void register(User user) throws ProjectException {
        UserDao userDAO = new UserDao();
        try {
            user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
            userDAO.create(user);
            user.setPassword(null);
        } catch (ProjectException e) {
            LOGGER.error("Registration user error" + e);
            throw new ProjectException("Service error", e);
        }
    }
}