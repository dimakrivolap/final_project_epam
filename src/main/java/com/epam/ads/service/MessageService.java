package com.epam.ads.service;

import com.epam.ads.dao.MessageDao;
import com.epam.ads.entity.Message;
import com.epam.ads.exception.ProjectException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
/**
 * MessageService.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class MessageService {
    private static final Logger LOGGER = LogManager.getLogger();

    public List<Message> getReseivedMessages(int idUser, int start) throws ProjectException {
        MessageDao messageDao = new MessageDao();
        List<Message> messages;
        try {
            messages = messageDao.findReseivedMessagesByIdUser(idUser, start);
        } catch (ProjectException e) {
            LOGGER.error("Message service error");
            throw new ProjectException("Service error", e);
        }
        return messages;
    }

    public List<Message> getSentMessages(int idUser, int start) throws ProjectException {
        MessageDao messageDao = new MessageDao();
        List<Message> messages;
        try {
            messages = messageDao.findSentMessagesByIdUser(idUser, start);
        } catch (ProjectException e) {
            LOGGER.error("Message service error");
            throw new ProjectException("Service error", e);
        }
        return messages;
    }

    public int getCountReceivedMessages(Integer idUser) throws ProjectException {
        int count = 0;
        MessageDao messageDao = new MessageDao();
        try {
            count = messageDao.findCountReceivedMessagesByIdUser(idUser);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return count;
    }

    public int getCountSentMessages(Integer idUser) throws ProjectException {
        int count = 0;
        MessageDao messageDao = new MessageDao();
        try {
            count = messageDao.findCountSentMessagesByIdUser(idUser);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
        return count;
    }

    public void createMessage(int idSender, int idReceiver, String message) throws ProjectException {
        MessageDao messageDao = new MessageDao();
        try {
            messageDao.createMessage(idSender, idReceiver, message);
        } catch (ProjectException e) {
            LOGGER.error("Post service error");
            throw new ProjectException("Service error", e);
        }
    }
}
