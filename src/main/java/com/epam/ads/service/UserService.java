package com.epam.ads.service;

import com.epam.ads.dao.UserDao;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * UserService.
 * @author Dmitry Krivolap
 * @version 1.0 30 Sep 2018
 */
public class UserService {
    private static final Logger LOGGER = LogManager.getLogger();

    public void updateProfile(User user) throws ProjectException {
        UserDao userDAO = new UserDao();
        try {
            user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
            userDAO.update(user);
            user.setPassword(null);
        } catch (ProjectException e) {
            LOGGER.error("Update profile user error" + e);
            throw new ProjectException("Service error", e);
        }
    }

    public User checkUser(String login, String password) throws ProjectException {
        User user;
        try {
            UserDao userDAO = new UserDao();
            user = userDAO.findUserByLogin(login).get();
            String currentPassword = DigestUtils.sha256Hex(password);

            boolean status = currentPassword.equals(user.getPassword()) && login.equals(user.getLogin());
            user.setPassword(null);
            if (!status) {
                user = null;
            }
        } catch (ProjectException e) {
            LOGGER.error("Check user error" + e);
            throw new ProjectException("Service error", e);
        }
        return user;
    }

    public void updateLocale(User user, String locale) throws ProjectException {
        UserDao userDAO = new UserDao();
        try {
            userDAO.updateLocale(user, locale);
            user.setPassword(null);
        } catch (ProjectException e) {
            LOGGER.error("Update profile user error" + e);
            throw new ProjectException("Service error", e);
        }
    }

    public User findUser(Integer idUser) throws ProjectException {
        User user;
        try {
            UserDao userDAO = new UserDao();
            user = userDAO.findById(new Integer(idUser)).get();
            user.setPassword(null);
        } catch (ProjectException e) {
            LOGGER.error("Service user error" + e);
            throw new ProjectException("Service error", e);
        }
        return user;
    }


    public User findUser(String loginReceiver) throws ProjectException {
        User user;
        try {
            UserDao userDAO = new UserDao();
            user = userDAO.findUserByLogin(loginReceiver).get();
        } catch (ProjectException e) {
            LOGGER.error("Service user error" + e);
            throw new ProjectException("Service error", e);
        }
        return user;
    }

    public void updatePassword(String email, String newPassword) throws ProjectException {
        UserDao userDAO = new UserDao();
        try {
            String password = DigestUtils.sha256Hex(newPassword);
            userDAO.updatePassword(email, password);
        } catch (ProjectException e) {
            LOGGER.error("Update profile user error" + e);
            throw new ProjectException("Service error", e);
        }
    }

    public void updateAvailable(int idUser, boolean available) throws ProjectException {
        UserDao userDAO = new UserDao();
        try {
            if (!TypeUser.ADMIN.equals(userDAO.findById(idUser).get().getTypeUser())) {
                userDAO.updateAvailable(idUser, available);
            } else {
                LOGGER.info("can not change admin's available");
            }
        } catch (ProjectException e) {
            LOGGER.error("Update available user error" + e);
            throw new ProjectException("Service error", e);
        }
    }
}
