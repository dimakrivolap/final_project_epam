package by.epam.connection;

import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.exception.ProjectException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionTest {
    private ConnectionPool pool;

    @BeforeClass
    public void initPool() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            pool = ConnectionPool.getInstance();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConnection() {
        try {
            Connection cn;
            for (int i = 0; i < 100; i++) {
                cn = pool.takeConnection();
                System.out.println(i + " : " + cn);
                pool.releaseConnection(cn);
            }

        } catch (ProjectException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void testMaxSizePoolConnection() {
        try {
            List<Connection> list = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                Connection connection = pool.takeConnection();
                list.add(connection);
                System.out.println(connection);

            }
            for (int i = 0; i < 10; i++) {
                System.out.println(i + " : " + list.get(i));
                pool.releaseConnection(list.get(i));
            }

        } catch (ProjectException e) {
            e.printStackTrace();
        }
        finally {
            pool.dispose();
        }
        return;

    }

}
