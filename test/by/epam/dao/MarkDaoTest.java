package by.epam.dao;

import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.dao.MarkDao;
import com.epam.ads.dao.PostDao;
import com.epam.ads.entity.Mark;
import com.epam.ads.entity.Post;
import com.epam.ads.exception.ProjectException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class MarkDaoTest {

        MarkDao markDao = new MarkDao();

    @BeforeClass
    public void initPool(){
        ConnectionPool.getInstance().initPool();
    }

        @Test void findMarksByIdUserTest(){

            try {
                List<Mark> marks = markDao.findMarksByIdUser(1);
                for (Mark mark :marks) {
                    System.out.println(mark.toString());
                }
            } catch (ProjectException e) {
                e.printStackTrace();
            }
        }

    }
