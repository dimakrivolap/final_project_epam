package by.epam.dao;


import com.epam.ads.dao.PostDao;
import com.epam.ads.entity.Post;
import com.epam.ads.exception.ProjectException;
import org.testng.annotations.Test;

import java.util.List;

public class PostDaoTest {
    PostDao postDao = new PostDao();
    @Test
    public void createPost(){
        Post post = new Post();
    }

    @Test
    public void findAllPosts(){
        try {
            List<Post> posts = postDao.findAll();
            for (Post p :posts) {
                System.out.println(p.toString());
                System.out.println();
            }
        } catch (ProjectException e) {
            e.printStackTrace();
        }
    }

    @Test void findPostsByUserId(){
        try {
            List<Post> posts = postDao.findByUserId(1);
            for (Post p :posts) {
                System.out.println(p.toString());
                System.out.println();
            }
        } catch (ProjectException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void findPostsByLogin(){
        try {
            List<Post> posts = postDao.findByLogin("dimak");
            for (Post p :posts) {
                System.out.println(p.toString());
                System.out.println();
            }
        } catch (ProjectException e) {
            e.printStackTrace();
        }
    }
}
