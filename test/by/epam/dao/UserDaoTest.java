package by.epam.dao;



import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.dao.UserDao;
import com.epam.ads.entity.TypeUser;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;


public class UserDaoTest {
    UserDao userDAO = new UserDao();
    @BeforeClass
    public void initPool(){
        ConnectionPool.getInstance().initPool();
    }

    @Test
    public void findByIdTest() throws ProjectException {

        Optional<User> user = userDAO.findById(2);
        System.out.println(user.toString());
    }

    @Test
    public void findAllTest() throws ProjectException {
        List<User> list = userDAO.findAll();
        for (User user: list) {
            System.out.println(user.toString());
        }
    }
    @Test
    public void createUserTest(){
        User user = new User(30,TypeUser.CLIENT,"login","password","first_name","last_name","email","371211234567");
        try {
            boolean result = userDAO.create(user);
            User userSelect = userDAO.findUserByLogin("login").get();
        }
        catch (ProjectException e){
            Assert.fail();
        }
    }

    @Test
    public void deleteUserByID(){
        User user = new User(30);
        try {

            userDAO.delete(user);
        }
        catch (ProjectException e){

        }
    }


}
