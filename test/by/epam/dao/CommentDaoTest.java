package by.epam.dao;

import com.epam.ads.connection.ConnectionPool;
import com.epam.ads.dao.CommentDao;
import com.epam.ads.entity.Comment;
import com.epam.ads.entity.User;
import com.epam.ads.exception.ProjectException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Optional;

public class CommentDaoTest {
    CommentDao commentDao = new CommentDao();
    @BeforeClass
    public void initPool(){
        ConnectionPool.getInstance().initPool();
    }


    @Test
    public void findCommentsByPostId() throws ProjectException {
        List<Comment> list = commentDao.findCommentsByPostId(4);
        for (Comment comment: list) {
            System.out.println(comment.toString());
        }
    }
}
